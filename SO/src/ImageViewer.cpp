#include "ImageViewer.h"
#include "QFile.h"

ImageViewer::ImageViewer(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::ImageViewerClass)
{
    ui->setupUi(this);
    ui->horizontalSlider->setVisible(false);
    ui->timesteps->setVisible(false);
    ui->hvalue->setVisible(false);
    ui->tauvalue->setVisible(false);
    ui->omegavalue->setVisible(false);
    ui->SOR->setVisible(false);
    ui->PeronaMalik->setVisible(false);
    ui->Explicit->setVisible(false);



    ui->timestepslabel->setVisible(false);
    ui->hlabel->setVisible(false);
    ui->taulabel->setVisible(false);
    ui->omegalabel->setVisible(false);
    ui->pushButton->setVisible(false);





}

//ViewerWidget functions
ViewerWidget* ImageViewer::getViewerWidget(int tabId)
{
    QScrollArea* s = static_cast<QScrollArea*>(ui->tabWidget->widget(tabId));
    if (s) {
        ViewerWidget* vW = static_cast<ViewerWidget*>(s->widget());
        return vW;
    }
    return nullptr;
}
ViewerWidget* ImageViewer::getCurrentViewerWidget()
{
    return getViewerWidget(ui->tabWidget->currentIndex());
}

// Event filters
bool ImageViewer::eventFilter(QObject* obj, QEvent* event)
{
    if (obj->objectName() == "ViewerWidget") {
        return ViewerWidgetEventFilter(obj, event);
    }
    return false;
}

//ViewerWidget Events
bool ImageViewer::ViewerWidgetEventFilter(QObject* obj, QEvent* event)
{
    ViewerWidget* w = static_cast<ViewerWidget*>(obj);

    if (!w) {
        return false;
    }

    if (event->type() == QEvent::MouseButtonPress) {
        ViewerWidgetMouseButtonPress(w, event);
    }
    else if (event->type() == QEvent::MouseButtonRelease) {
        ViewerWidgetMouseButtonRelease(w, event);
    }
    else if (event->type() == QEvent::MouseMove) {
        ViewerWidgetMouseMove(w, event);
    }
    else if (event->type() == QEvent::Leave) {
        ViewerWidgetLeave(w, event);
    }
    else if (event->type() == QEvent::Enter) {
        ViewerWidgetEnter(w, event);
    }
    else if (event->type() == QEvent::Wheel) {
        ViewerWidgetWheel(w, event);
    }

    return QObject::eventFilter(obj, event);
}
void ImageViewer::ViewerWidgetMouseButtonPress(ViewerWidget* w, QEvent* event)
{
    QMouseEvent* e = static_cast<QMouseEvent*>(event);
    if (e->button() == Qt::LeftButton) {
        w->setFreeDrawBegin(e->pos());
        w->setFreeDrawActivated(true);
    }
}
void ImageViewer::ViewerWidgetMouseButtonRelease(ViewerWidget* w, QEvent* event)
{
    QMouseEvent* e = static_cast<QMouseEvent*>(event);
    if (e->button() == Qt::LeftButton && w->getFreeDrawActivated()) {
        w->freeDraw(e->pos(), QPen(Qt::red));
        w->setFreeDrawActivated(false);
    }
}
void ImageViewer::ViewerWidgetMouseMove(ViewerWidget* w, QEvent* event)
{
    QMouseEvent* e = static_cast<QMouseEvent*>(event);
    if (e->buttons() == Qt::LeftButton && w->getFreeDrawActivated()) {
        w->freeDraw(e->pos(), QPen(Qt::red));
        w->setFreeDrawBegin(e->pos());
    }
}
void ImageViewer::ViewerWidgetLeave(ViewerWidget* w, QEvent* event)
{
}
void ImageViewer::ViewerWidgetEnter(ViewerWidget* w, QEvent* event)
{
}
void ImageViewer::ViewerWidgetWheel(ViewerWidget* w, QEvent* event)
{
    QWheelEvent* wheelEvent = static_cast<QWheelEvent*>(event);
}

//ImageViewer Events
void ImageViewer::closeEvent(QCloseEvent* event)
{
    if (QMessageBox::Yes == QMessageBox::question(this, "Close Confirmation", "Are you sure you want to exit?", QMessageBox::Yes | QMessageBox::No))
    {
        event->accept();
    }
    else {
        event->ignore();
    }
}

//Image functions
void ImageViewer::openNewTabForImg(ViewerWidget* vW)
{
    QScrollArea* scrollArea = new QScrollArea;
    scrollArea->setObjectName("QScrollArea");
    scrollArea->setWidget(vW);

    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidgetResizable(true);
    scrollArea->installEventFilter(this);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    vW->setObjectName("ViewerWidget");
    vW->installEventFilter(this);

    QString name = vW->getName();

    ui->tabWidget->addTab(scrollArea, name);

}
bool ImageViewer::openImage(QString filename)
{
    QFileInfo fi(filename);

    QString name = fi.baseName();
    openNewTabForImg(new ViewerWidget(name, QSize(0, 0)));
    ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);

    ViewerWidget* w = getCurrentViewerWidget();

    QImage loadedImg(filename);
    return w->setImage(loadedImg);
}
bool ImageViewer::saveImage(QString filename)
{
    QFileInfo fi(filename);
    QString extension = fi.completeSuffix();
    ViewerWidget* w = getCurrentViewerWidget();

    QImage* img = w->getImage();
    return img->save(filename, extension.toStdString().c_str());
}
bool ImageViewer::clearImage()
{
    ViewerWidget* w = getCurrentViewerWidget();
    w->clear();
    w->update();
    return true;
}
bool ImageViewer::invertColors()
{
    ViewerWidget* w = getCurrentViewerWidget();
    uchar* data = w->getData();

    int row = w->getImage()->bytesPerLine();
    int depth = w->getImage()->depth();


    for (int i = 0; i < w->getImgHeight(); i++)
    {
        for (int j = 0; j < w->getImgWidth(); j++)
        {
            if (depth == 8) {
                w->setPixel(j, i, static_cast<uchar>(255 - data[i * row + j ]));
            }
            else {
                uchar r = static_cast<uchar>(255 - data[i * row + j * 4]);
                uchar g = static_cast<uchar>(255 - data[i * row + j * 4 + 1]);
                uchar b = static_cast<uchar>(255 - data[i * row + j * 4 + 2]);
                w->setPixel(j, i, r, g, b);
            }

        }
    }

    w->update();
    return true;
}

bool ImageViewer::Mirroring(int n,bool b){

    ViewerWidget* w = getCurrentViewerWidget();
    w->setUpdatesEnabled(true);
    uchar* data;
    QImage loadedImg = *(w->getImage()),img1;
    int row=loadedImg.bytesPerLine();
    img1=QImage(QSize(loadedImg.width()+2*n,loadedImg.height()+2*n), QImage::Format_Grayscale8);
    w->setImage(img1);
    data = loadedImg.bits();

    //na to aby ked mirroring pouzivam bez vykreslenia(iba ako pomoc, tak aby sa mi zbytocne neupdatoval a neprekresloval widget)
    if(b==true)
        w->setUpdatesEnabled(true);

    else
        w->setUpdatesEnabled(false);


    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {

            w->setPixel(j, i, (static_cast<uchar>(data[(n-i) * row + n-j ])));//lavy horny
            w->setPixel(loadedImg.width()+2*n-j-1, i, (static_cast<uchar>(data[(n-i) * row +j+loadedImg.width()-n-1 ])));//dolny lavy
            w->setPixel(loadedImg.width()+2*n-1-j, loadedImg.height()+2*n-1-i, (static_cast<uchar>(data[(loadedImg.height()-n+i-1) * row + loadedImg.width()-n+j-1 ])));//pravy horny
            w->setPixel(j, loadedImg.height()+2*n-i-1, (static_cast<uchar>(data[(loadedImg.height()-n+i-1) * row + n-j ])));//pravy dolny

        }
    }

    for (int i = 0; i < loadedImg.height(); i++)
    {
        for (int j = 0; j < n; j++)
        {

            w->setPixel(j, n+i, (static_cast<uchar>(data[i * row + n-j ])));
            w->setPixel(n+j+loadedImg.width(), n+i, (static_cast<uchar>(data[i * row + loadedImg.width()-1-j ])));

        }
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < loadedImg.width(); j++)
        {

            w->setPixel(n+j, i,(static_cast<uchar>(data[(n-i) * row + j ])));
            w->setPixel(n+j, i+loadedImg.height()+n, (static_cast<uchar>(data[(loadedImg.height()-i-1) * row +j ])));

        }
    }




    //do stredu dam ten obrazok
    for (int i = 0; i < loadedImg.height(); i++)
    {
        for (int j = 0; j < loadedImg.width(); j++)
        {
                w->setPixel(j+n, i+n, (static_cast<uchar>(data[i * row + j])));
        }
    }

    w->update();

    return true;


}

double * ImageViewer::mirror(int n,double* img,int x, int y){

    double *img1;
    img1=new double[(x+2*n)*(y+2*n)];

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {

            img1[i*x+j] = img[(n-i) * x + n-j ];
            img1[i*x+x+2*n-j-1] = img[(n-i) * x +j+x-n-1 ];
            img1[(y+2*n-1-i)*x+x+2*n-1-j] = img[(y-n+i-1) * x + x-n+j-1 ];
            img1[(y+2*n-i-1)*x+j] = img[(y-n+i-1) * x + n-j ];
        }
    }

    for (int i = 0; i < y; i++)
    {
        for (int j = 0; j < n; j++)
        {

            img1[(n+i)*x+j]=img[i * x + n-j ];
            img1[(n+i)*x+n+j+x]=img[i * x + x-1-j ];

        }
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < x; j++)
        {

            img1[(i)*x+n+j]=img[(n-i) * x + j ];
            img1[(i+y+n)*x+n+j]=img[(y-i-1) * x +j ];

        }
    }




    //do stredu dam ten obrazok
    for (int i = 0; i < y; i++)
    {
        for (int j = 0; j <x; j++)
        {


            img1[(i+n)*x+j+n]=img[i * x + j];

        }
    }


    return img1;

}

int* ImageViewer::histogram(bool b){

    ViewerWidget* w = getCurrentViewerWidget(),*w1;
    uchar* data;
    QImage img1,loadedImg = *w->getImage();
    int row=loadedImg.bytesPerLine(),*hist,max=-10000000000;
    w->setUpdatesEnabled(true);


    data = loadedImg.bits();
    hist = new int[256];

    if(b==true){
        img1=QImage(QSize(1000,1000), QImage::Format_Grayscale8);

        openNewTabForImg(new ViewerWidget("histogram", QSize(0, 0)));
        ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);

        w1 = getCurrentViewerWidget();
        w1->setImage(img1);


        for (int j = 0 ;j < 1000; j++) {
            for (int i = 0 ;i < 1000; i++) {

                w1->setPixel(i,j,static_cast<uchar>(255));
            }


        }

    }

    for (int j=0;j<256;j++) {

        hist[j]=0;

    }

    for (int j = 0 ;j < loadedImg.height(); j++) {
        for (int i = 0 ;i < loadedImg.width(); i++) {

            hist[int(data[j * row + i])] = hist[int(data[j * row + i])] + 1 ;

        }


    }

    if(b==true){

        for (int j=0;j<256;j++) {

           if(hist[j]>max)
               max=hist[j];

        }



        for (int j=0;j<256;j++) {

            for(int i = 0; i<((int)((500./(double)max)*hist[j])); i++){

                w1->setPixel(10+3*j,500-i,static_cast<uchar>(0));

            }

        }

        w1->update();

    }

    return hist;


}

double* ImageViewer::histogramN(bool b){

    ViewerWidget* w = getCurrentViewerWidget(),*w1;
    uchar* data;
    QImage img1,loadedImg = *w->getImage();
    int *hist;
    double *histN = new double[256],max=-10000000000;
    w->setUpdatesEnabled(true);

    data = loadedImg.bits();
    //false je aby nevykreslil histogram ale vratil iba hodnoty
    hist = histogram(false);

    //true aby aj kreslil
    if(b==true){

        img1=QImage(QSize(1000,1000), QImage::Format_Grayscale8);

        openNewTabForImg(new ViewerWidget("histogram Normal", QSize(0, 0)));
        ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);

        w1 = getCurrentViewerWidget();
        w1->setImage(img1);




        for (int j = 0 ;j < 1000; j++) {
            for (int i = 0 ;i < 1000; i++) {

                w1->setPixel(i,j,static_cast<uchar>(255));
            }


        }
    }

    for (int j=0;j<256;j++) {

        histN[j]=double(1./(loadedImg.height()*loadedImg.width()))*(double)hist[j];

    }


    if(b==true){
        for (int j=0;j<256;j++) {
            //tu iba skalujem hodnoty aby ich bolo vidno pri vykreselni
           if(500*histN[j]>max)
               max=500*histN[j];

        }



        for (int j=0;j<256;j++) {

            for(int i = 0; i<((int)((500./(double)max)*(double)histN[j]*500.)); i++){

                w1->setPixel(10+3*j,500-i,static_cast<uchar>(0));

            }

        }

        w1->update();

    }

    return histN;


}

bool ImageViewer::fshs(){

    ViewerWidget* w = getCurrentViewerWidget();
    uchar* data;
    int row = w->getImage()->bytesPerLine(),*hist,A=0,B=0,indx1=0,indx2=0,min = 10000000,max =-100000000;
    data = w->getData();
    w->setUpdatesEnabled(true);

    //false je aby nevykreslil histogram ale vratil iba hodnoty
    hist = histogram(false);
    //hladam min zlava a zprava
    while(true){

        if((A!=0)&&(B!=0)){

            break;
        }
        else{

            if((hist[indx1]!=0)&&(A==0))
                A=indx1;
            if((hist[255-indx2]!=0)&&(B==0))
                B=255-indx2;

            indx1++;
            indx2++;

        }

    }

    for (int i = 0; i < w->getImgHeight(); i++)
    {
        for (int j = 0; j < w->getImgWidth(); j++)
        {
                w->setPixel(j, i, static_cast<uchar>(int(double((255.)/double(B-A))*double(data[i * row + j]-A))));
        }
    }


    w->update();

    return true;


}

double* ImageViewer::loadmask(QString filename){

    QFile file(filename);
    int n,i=0;
    double *maska;

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return 0;

    QTextStream in(&file);

    n = in.readLine().toInt();
    maska = new double[(n*n)+2];

    maska[i]=n;
    i++;

    while (!in.atEnd()) {
        QString line = in.readLine();
        maska[i] = line.toDouble();
        i++;
    }


    return maska;

}

bool ImageViewer::convolution(int n,QString filename){


    ViewerWidget* w = getCurrentViewerWidget();
    QImage img1,img = *w->getImage();
    uchar* data = img.bits();
    int row=w->getImage()->bytesPerLine();
    img1=QImage(QSize(w->getImgWidth()-2*n,w->getImgHeight()-2*n), QImage::Format_Grayscale8);
    w->setImage(img1);
    double *maska,suma=0.;
    w->setUpdatesEnabled(true);

    maska = loadmask(filename);

    for (int i = 0; i < img1.height(); i++)
    {
        for (int j = 0; j < img1.width(); j++)
        {

            suma = 0.;
            for(int k = 0; k < (int)maska[0];k++){

                for(int k1 = 0; k1 < (int)maska[0];k1++){

                    suma = suma + maska[k*(int)maska[0] + k1+1]*((int)(data[(i+n-1+k-int((int)maska[0]/2.)) * row + j+n-1+k1-int((int)maska[0]/2.) ]));

                }

            }

            w->setPixel(j, i, (static_cast<uchar>(suma)));



        }
    }

    w->update();

    return true;


}

int ImageViewer::mean(int *hist,int a,int b,int N){

    double m=0.;

    for (int i = a; i <= b; i++) {

        m = m + i*hist[i];

    }

    return int(double((1./(double)N)*m));


}

int ImageViewer::count(int *hist,int a,int b){

    double m=0.;

    for (int i = a; i <= b; i++) {

        m = m + hist[i];

    }

    return int(m);


}

int ImageViewer::isodata(int *hist){

    ViewerWidget* w = getCurrentViewerWidget();
    uchar* data = w->getData();

    int row = w->getImage()->bytesPerLine();
    int depth = w->getImage()->depth(),i=0;
    w->setUpdatesEnabled(true);

    int K=256,q=0,N=0,ql=-100,n0=0,n1=0,mi0=0,mi1=0;

    N = w->getImgWidth()*w->getImgHeight();
    q = mean(hist,0,K-1,N);

    do {

        n0=count(hist,0,q);
        n1=count(hist,q+1,K-1);
        if((n0==0)||(n1==0))
            return -1;

        mi0=mean(hist,0,q,n0);
        mi1=mean(hist,q+1,K-1,n1);
        ql=q;

        q=(mi0+mi1)/2;
        i++;

    } while (q!=ql);

    qDebug()<<"q: "<<q<<"pocet iteracii: "<<i<<endl;

    for (int i = 0; i < w->getImgHeight(); i++)
    {
        for (int j = 0; j < w->getImgWidth(); j++)
        {
            if(data[i * row + j]<=q)
                w->setPixel(j, i, static_cast<uchar>(0.));
            else
                w->setPixel(j, i, static_cast<uchar>(255.));
        }
    }

    w->update();

    return q;

}

int ImageViewer::max(int *maska,int size){

    int max=-1000000;

    for (int i = 0; i < size; i++) {

        if(maska[i]>max)
            max=maska[i];
    }

    return max;
}

int ImageViewer::min(int *maska,int size){

    int min=1000000;

    for (int i = 0; i < size; i++) {

        if(maska[i]<min)
            min=maska[i];
    }

    return min;
}

bool ImageViewer::bernsen(int n, int r, int cmin, int bg){


    ViewerWidget* w = getCurrentViewerWidget();
    w->setUpdatesEnabled(true);
    QImage img1,img = *w->getImage();
    uchar* data = img.bits();
    int row=w->getImage()->bytesPerLine(),*maska = new int [r*r],K=256,ql=0,Imax=0,Imin=0,c=0,pixCol=0,bcol=0,icol=0;
    img1=QImage(QSize(w->getImgWidth()-2*n,w->getImgHeight()-2*n), QImage::Format_Grayscale8);
    w->setImage(img1);

    if(bg==0){
        ql=K;
        bcol=0;
        icol=255;
    }
    else{
        ql=0;
        bcol=255;
        icol=0;
    }



    for (int i = 0; i < img1.height(); i++)
    {
        for (int j = 0; j < img1.width(); j++)
        {

            for(int k = 0; k < r;k++){

                for(int k1 = 0; k1 < r;k1++){

                    //maska[k*r + k1] = ((int)(data[(i+n-1+k-int(r/1.)) * row + j+n-1+k1-int(r/1.) ]));
                    maska[k*r + k1] = ((int)(data[(i+n-1+k-int(r/2.)) * row + j+n-1+k1-int(r/2.) ]));

                }

            }

            Imax=max(maska,r*r);
            Imin=min(maska,r*r);
            c=Imax-Imin;

            if(c>=cmin){
                pixCol = (Imin+Imax)/2;
                if((((int)(data[(i+n-1) * row + j+n-1 ]))<pixCol)&&(bg==0))
                    w->setPixel(j, i, (static_cast<uchar>(bcol)));
                else if((((int)(data[(i+n-1) * row + j+n-1 ]))>=pixCol)&&(bg==0))
                    w->setPixel(j, i, (static_cast<uchar>(icol)));
                else if((((int)(data[(i+n-1) * row + j+n-1 ]))>pixCol)&&(bg==1))
                    w->setPixel(j, i, (static_cast<uchar>(bcol)));
                else if((((int)(data[(i+n-1) * row + j+n-1 ]))<=pixCol)&&(bg==1))
                    w->setPixel(j, i, (static_cast<uchar>(icol)));
            }
            else{
                pixCol = ql;
                 w->setPixel(j, i, (static_cast<uchar>(bcol)));
            }

        }
    }



    w->update();

    return true;

}

QImage* ImageViewer::heatE(int poc, double h, double tau){


    if(tau>(h*h)/4.){
        tau=(h*h)/4.;
    }

    ViewerWidget* w = getCurrentViewerWidget();
    w->setUpdatesEnabled(true);
    QImage img1,img = *w->getImage(),*sol;
    uchar* dat0 = img.bits();
    double *u0,*un;
    int row=w->getImage()->bytesPerLine(),x1=w->getImgWidth(),x2=w->getImgHeight();

    un = new double[x1*x2];
    u0 = new double[x1*x2];

    for(int p=0;p<(x1*x2);p++)
        u0[p]=(double)dat0[p];

    sol = new QImage[poc];
    double sum=0,pocs=0;

    for(int i = 0; i<poc; i++){

        sol[i]=QImage(QSize(x1,x2), QImage::Format_Grayscale8);

    }

    for (int j = 0; j < x2; j++)
    {
        for (int i = 0; i < x1; i++)
        {
//            QColor col;
//            col.setRgb(u0[x1*j + i],u0[x1*j + i],u0[x1*j + i]);
//            sol[0].setPixelColor(i,j,col);
            sol[0].bits()[x1*j + i]=static_cast<uchar>(u0[x1*j+i]+0.5);

        }
    }


    for(int m=1; m<poc;m++){

        for (int j = 0; j < x2; j++)
        {
            for (int i = 0; i < x1; i++)
            {
                sum=0;pocs=0;
                if(i>0){
                    pocs++;
                    sum=sum+u0[x1*j + i-1];
                }
                if(i<(x1-1)){
                    pocs++;
                    sum=sum+u0[x1*j + i+1];
                }
                if(j>0){
                    pocs++;
                    sum=sum+u0[x1*(j-1) + i];
                }
                if(j<(x2-1)){
                    pocs++;
                    sum=sum+u0[x1*(j+1) + i];
                }

                un[x1*j + i] = (1. - (tau/(h*h))*pocs )*u0[x1*j + i] + sum*(tau/(h*h));

//                QColor col;
//                col.setRgb(un[x1*j + i],un[x1*j + i],un[x1*j + i]);
//                sol[m].setPixelColor(i,j,col);
                sol[m].bits()[x1*j + i]=static_cast<uchar>(un[x1*j+i]+0.5);

            }

        }

        for(int p=0;p<(x1*x2);p++)
            u0[p]=un[p];
    }

    return sol;

}

QImage* ImageViewer::heatSOR(int poc, double h, double tau,double omega){

    ViewerWidget* w = getCurrentViewerWidget();
    w->setUpdatesEnabled(true);
    QImage img1,img = *w->getImage(),*sol;
    uchar* dat0 = img.bits();
    double *u0,*un,*unn,apq = (tau)/(h*h),y=0,*R,RR=0;
    int row=w->getImage()->bytesPerLine(),x1=w->getImgWidth(),x2=w->getImgHeight(),l = 0;
    bool b = true;
    double tol = 1e-8;

    un = new double[x1*x2];
    unn = new double[x1*x2];
    u0 = new double[x1*x2];
    R = new double[x1*x2];
    for(int p=0;p<(x1*x2);p++){
        u0[p]=(double)dat0[p];
        un[p]=u0[p];
        unn[p]=0.;
        R[p]=0.;
    }

    sol = new QImage[poc];
    double suml=0,suml1=0,pocs=0;

    for(int i = 0; i<poc; i++){

        sol[i]=QImage(QSize(x1,x2), QImage::Format_Grayscale8);

    }
    for (int j = 0; j < x2; j++)
    {
        for (int i = 0; i < x1; i++)
        {
//            QColor col;
//            col.setRgb(u0[x1*j + i],u0[x1*j + i],u0[x1*j + i]);
//            sol[0].setPixelColor(i,j,col);
            sol[0].bits()[x1*j + i]=static_cast<uchar>(u0[x1*j+i]+0.5);

        }
    }
    qDebug()<<"u10:="<<u0[50]<<endl;

    for(int m=1; m<poc;m++){
        l = 0;
        b=true;
        while (b==true) {
            RR=0;
            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {


                    suml=0;suml1=0;pocs=0;
                    if(i>0){
                        pocs++;
                        suml1=suml1+apq*unn[x1*j + i-1];

                    }
                    if(i<(x1-1)){
                        pocs++;
                        suml=suml+apq*un[x1*j + i+1];
                    }
                    if(j>0){
                        pocs++;
                        suml1=suml1+apq*unn[x1*(j-1) + i];
                    }
                    if(j<(x2-1)){
                        pocs++;
                        suml=suml+apq*un[x1*(j+1) + i];
                    }

                    y = (u0[x1*j + i] + suml1 + suml)/(1.0+pocs*apq);
                    unn[x1*j + i] = un[x1*j + i] + omega*(y - un[x1*j + i]);

                }


            }

            l++;

            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {

                    suml1=0;pocs=0;
                    if(i>0){
                        pocs++;
                        suml1=suml1+apq*unn[x1*j + i-1];

                    }
                    if(i<(x1-1)){
                        pocs++;
                        suml1=suml1+apq*unn[x1*j + i+1];
                    }
                    if(j>0){
                        pocs++;
                        suml1=suml1+apq*unn[x1*(j-1) + i];
                    }
                    if(j<(x2-1)){
                        pocs++;
                        suml1=suml1+apq*unn[x1*(j+1) + i];
                    }

                    R[x1*j + i] = (1+(pocs*apq))*unn[x1*j + i] - (suml1) - u0[x1*j + i];
                    RR=RR+(R[x1*j + i]*R[x1*j + i]);

                }
            }


            for(int p=0;p<(x1*x2);p++){
                un[p]=unn[p];
                unn[p]=0.;
            }

            //qDebug()<<"m:="<<m<<" l:="<<l<<endl;

            if((sqrt(RR)<tol)||(l>500)){
                b=false;
            }

        }


        for (int j = 0; j < x2; j++)
        {
            for (int i = 0; i < x1; i++)
            {

                sol[m].bits()[x1*j + i]=static_cast<uchar>(un[x1*j+i]+0.5);

            }
        }

        //previerka ci sa zachovava stredna hodnota vsetkych pixlov, mame izolovanu sustavu, cize nic neutieklo
//        double mean=0;

        for(int p=0;p<(x1*x2);p++){
            u0[p]=un[p];
            //mean=mean+un[p];
        }


//        qDebug()<<"mean:="<<mean/(x1*x2)<<endl;

    }

    return sol;

}

double ImageViewer::gFunction(double s){

    double K=0.02,res=0.;

    res = 1./(1.+(K*s*s));

    return res;
}


double ImageViewer::gradNorm(double ynw,double yne,double ysw,double yse,double xw,double xe,double h){

    double ux=0.,uy=0.,res=0.;

    ux = (xe - xw)/h;
    uy = (ynw+yne-ysw-yse)/(4*h);
    res = sqrt((ux*ux) + (uy*uy));

    return res;
}

QImage* ImageViewer::perona_malikE(int poc, double h, double tau){
    if(tau>(h*h)/4.){
        tau=(h*h)/4.;
    }

    ViewerWidget* w = getCurrentViewerWidget();
    w->setUpdatesEnabled(true);
    QImage img1,img = *w->getImage(),*sol;
    uchar* dat0 = img.bits();
    double *u0,*un;
    int x1=w->getImgWidth(),x2=w->getImgHeight(),y1=x1,y2=x2;

    un = new double[x1*x2];
    u0 = new double[x1*x2];
    //gs = new double[x1*x2];


    for(int p=0;p<(x1*x2);p++)
        u0[p]=(double)dat0[p];

    sol = new QImage[poc];
    double sum=0,pocs=0,gpq=0.,gradnorm=0.,mean=0.;

    for(int i = 0; i<poc; i++){

        sol[i]=QImage(QSize(x1,x2), QImage::Format_Grayscale8);

    }

    for (int j = 0; j < x2; j++)
    {
        for (int i = 0; i < x1; i++)
        {
            sol[0].bits()[x1*j + i]=static_cast<uchar>(u0[x1*j+i]+0.5);
        }
    }

    for(int m=1; m<poc;m++){

        for (int j = 0; j < y2; j++)
        {
            for (int i = 0; i < y1; i++)
            {
                sum=0;gpq=0.;pocs=0.;

                if((i>0)&&(j>0)&&(j<(x2-1))){
                    pocs++;
                    gradnorm=gradNorm(u0[y1*(j+1) + i-1],u0[y1*(j+1) + i],u0[y1*(j-1) + i-1],u0[y1*(j-1) + i],u0[y1*(j) + i-1],u0[y1*(j) + i],h);
                    gpq = gpq+gFunction(gradnorm);
                    sum=sum+u0[y1*j + i-1]*gFunction(gradnorm);

                }
                if((i<(x1-1))&&(j>0)&&(j<(x2-1))){
                    pocs++;
                    gradnorm=gradNorm(u0[y1*(j+1) + i],u0[y1*(j+1) + i+1],u0[y1*(j-1) + i],u0[y1*(j-1) + i+1],u0[y1*(j) + i],u0[y1*(j) + i+1],h);
                    gpq = gpq+gFunction(gradnorm);
                    sum=sum+u0[y1*j + i+1]*gFunction(gradnorm);

                }
                if((j>0)&&(i>0)&&(i<(x1-1))){
                    pocs++;
                    gradnorm=gradNorm(u0[y1*(j) + i-1],u0[y1*(j-1) + i-1],u0[y1*(j) + i+1],u0[y1*(j-1) + i+1],u0[y1*(j) + i],u0[y1*(j-1) + i],h);
                    gpq = gpq+gFunction(gradnorm);
                    sum=sum+u0[y1*(j-1) + i]*gFunction(gradnorm);
                }
                if((j<(x2-1))&&(i>0)&&(i<(x1-1))){
                    pocs++;
                    gradnorm=gradNorm(u0[y1*(j+1) + i-1],u0[y1*(j) + i-1],u0[y1*(j+1) + i+1],u0[y1*(j) + i+1],u0[y1*(j+1) + i],u0[y1*(j) + i],h);
                    gpq = gpq+gFunction(gradnorm);
                    sum=sum+u0[y1*(j+1) + i]*gFunction(gradnorm);
                }
//                if(m==1)
//                gs[y1*j + i] = gpq;

                un[y1*j + i] = (1. - (tau/(h*h))*gpq )*u0[y1*j + i] + sum*(tau/(h*h));

                sol[m].bits()[x1*j + i]=static_cast<uchar>(un[x1*j+i]+0.5);

            }

        }

        mean=0.;
        for(int p=0;p<(y1*y2);p++){
            u0[p]=un[p];
            mean=mean+un[p];
        }

        qDebug()<<"meanPM:="<<mean/(y1*y2)<<endl;

    }

//    for (int j = 0; j < x2; j++)
//    {
//        for (int i = 0; i < x1; i++)
//        {
//            sol[poc-1].bits()[x1*j + i]=static_cast<uchar>(255*gs[x1*j+i]+0.5);
//        }
//    }


    return sol;

}


//QImage* ImageViewer::perona_malikE(int poc, double h, double tau){
//    if(tau>(h*h)/4.){
//        tau=(h*h)/4.;
//    }

//    ViewerWidget* w = getCurrentViewerWidget();
//    w->setUpdatesEnabled(true);
//    QImage img1,img = *w->getImage(),*sol;
//    uchar* dat0 = img.bits();
//    double *u0,*un,*datn0;
//    int x1=w->getImgWidth(),x2=w->getImgHeight(),y1=x1+2,y2=x2+2;

//    un = new double[x1*x2];
//    u0 = new double[y1*y2];
//    //gs = new double[x1*x2];


//    for(int p=0;p<(x1*x2);p++)
//        un[p]=static_cast<double>(dat0[p]+0.5);

//   datn0 = mirror(1,un,x1,x2);

//    for(int p=0;p<(y1*y2);p++)
//        u0[p]=static_cast<double>(datn0[p]+0.5);

////    for(int p=0;p<(x1*x2);p++){

////        qDebug()<<"un:="<<un[p]<<" u0:="<<u0[p+y1+1]<<endl;
////    }

//    for (int j = 1; j < y2-1; j++)
//    {
//        for (int i = 1; i < y1-1; i++)
//        {
//            qDebug()<<"un:="<<un[x1*(j-1) + i-1]<<" u0:="<<u0[y1*j+i]<<endl;
//        }
//    }


//    sol = new QImage[poc];
//    double sum=0,pocs=0,gpq=0.,gradnorm=0.,mean=0.;

//    for(int i = 0; i<poc; i++){

//        sol[i]=QImage(QSize(x1,x2), QImage::Format_Grayscale8);

//    }

//    for (int j = 1; j < y2-1; j++)
//    {
//        for (int i = 1; i < y1-1; i++)
//        {
//            sol[0].bits()[x1*(j-1) + i-1]=static_cast<uchar>(u0[y1*j+i]+0.5);
//        }
//    }

//    for(int m=1; m<poc;m++){

//        for (int j = 1; j < y2-1; j++)
//        {
//            for (int i = 1; i < y1-1; i++)
//            {
//                sum=0;gpq=0.;pocs=0.;

//                gradnorm=gradNorm(u0[y1*(j+1) + i-1],u0[y1*(j+1) + i],u0[y1*(j-1) + i-1],u0[y1*(j-1) + i],u0[y1*(j) + i-1],u0[y1*(j) + i],h);
//                gpq = gpq+gFunction(gradnorm);
//                sum=sum+u0[y1*j + i-1]*gFunction(gradnorm);

//                gradnorm=gradNorm(u0[y1*(j+1) + i],u0[y1*(j+1) + i+1],u0[y1*(j-1) + i],u0[y1*(j-1) + i+1],u0[y1*(j) + i],u0[y1*(j) + i+1],h);
//                gpq = gpq+gFunction(gradnorm);
//                sum=sum+u0[y1*j + i+1]*gFunction(gradnorm);

//                gradnorm=gradNorm(u0[y1*(j) + i-1],u0[y1*(j-1) + i-1],u0[y1*(j) + i+1],u0[y1*(j-1) + i+1],u0[y1*(j) + i],u0[y1*(j-1) + i],h);
//                gpq = gpq+gFunction(gradnorm);
//                sum=sum+u0[y1*(j-1) + i]*gFunction(gradnorm);

//                gradnorm=gradNorm(u0[y1*(j-1) + i-1],u0[y1*(j) + i-1],u0[y1*(j+1) + i+1],u0[y1*(j) + i+1],u0[y1*(j+1) + i],u0[y1*(j) + i],h);
//                gpq = gpq+gFunction(gradnorm);
//                sum=sum+u0[y1*(j+1) + i]*gFunction(gradnorm);

//                un[x1*(j-1) + i-1] = (1. - (tau/(h*h))*gpq )*u0[y1*j + i] + sum*(tau/(h*h));

//                sol[m].bits()[x1*(j-1) + i-1]=static_cast<uchar>(un[x1*(j-1) + i-1]+0.5);

//            }

//        }

////        mean=0.;
//        datn0 = mirror(1,un,x1,x2);
//        for(int p=0;p<(y1*y2);p++){
//            u0[p]=static_cast<double>(datn0[p]);
////            mean=mean+un[p];
//        }

////        qDebug()<<"mean:="<<mean/(y1*y2)<<endl;

//    }

////    for (int j = 0; j < x2; j++)
////    {
////        for (int i = 0; i < x1; i++)
////        {
////            sol[poc-1].bits()[x1*j + i]=static_cast<uchar>(255*gs[x1*j+i]+0.5);
////        }
////    }


//    return sol;

//}

double* ImageViewer::linearDiffusionSOR(double *u0, int x1, int x2, int poc, double h, double tau, double omega,bool ret){


    double *un,*unn,apq = (tau)/(h*h),y=0,*R,RR=0,*gs;
    int l = 0,y1=x1,y2=x2;
    bool b = true;
    double tol = 1e-3;

    un = new double[x1*x2];
    unn = new double[x1*x2];
    R = new double[x1*x2];
    gs = new double[4*x1*x2];

    for(int p=0;p<(x1*x2);p++){
        un[p]=u0[p];
        unn[p]=0.;
        R[p]=0.;
    }
    for(int p=0;p<(4*x1*x2);p++){
        gs[p]=0.;
    }

    double suml=0,suml1=0,pocs=0,sum=0,gpq=0.,gradnorm=0.;

    for(int m=1; m<poc;m++){
        l = 0;
        b=true;
        while (b==true) {
            RR=0;
            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {


                    suml=0;suml1=0;pocs=0;
                    if(i>0){
                        pocs++;
                        suml1=suml1+apq*unn[x1*j + i-1];

                    }
                    if(i<(x1-1)){
                        pocs++;
                        suml=suml+apq*un[x1*j + i+1];
                    }
                    if(j>0){
                        pocs++;
                        suml1=suml1+apq*unn[x1*(j-1) + i];
                    }
                    if(j<(x2-1)){
                        pocs++;
                        suml=suml+apq*un[x1*(j+1) + i];
                    }

                    y = (u0[x1*j + i] + suml1 + suml)/(1.0+pocs*apq);
                    unn[x1*j + i] = un[x1*j + i] + omega*(y - un[x1*j + i]);

                }


            }

            l++;

            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {

                    suml1=0;pocs=0;
                    if(i>0){
                        pocs++;
                        suml1=suml1+apq*unn[x1*j + i-1];

                    }
                    if(i<(x1-1)){
                        pocs++;
                        suml1=suml1+apq*unn[x1*j + i+1];
                    }
                    if(j>0){
                        pocs++;
                        suml1=suml1+apq*unn[x1*(j-1) + i];
                    }
                    if(j<(x2-1)){
                        pocs++;
                        suml1=suml1+apq*unn[x1*(j+1) + i];
                    }

                    R[x1*j + i] = (1+(pocs*apq))*unn[x1*j + i] - (suml1) - u0[x1*j + i];
                    RR=RR+(R[x1*j + i]*R[x1*j + i]);

                }
            }


            for(int p=0;p<(x1*x2);p++){
                un[p]=unn[p];
                unn[p]=0.;
            }

            //qDebug()<<"m:="<<m<<" l:="<<l<<endl;

            if((sqrt(RR)<tol)||(l>50)){
                b=false;
            }

        }

        for(int p=0;p<(x1*x2);p++){
            u0[p]=un[p];
        }

    }

    //vypocet 4 hodnot funckie g(s)
    if(ret==true){
        for (int j = 0; j < y2; j++)
        {
            for (int i = 0; i < y1; i++)
            {

                if((i>0)&&(j>0)&&(j<(x2-1))){
                    gradnorm=gradNorm(u0[y1*(j+1) + i-1],u0[y1*(j+1) + i],u0[y1*(j-1) + i-1],u0[y1*(j-1) + i],u0[y1*(j) + i-1],u0[y1*(j) + i],h);
                    gs[4*y1*j+4*i+0]=gFunction(gradnorm);//west

                }
                if((i<(x1-1))&&(j>0)&&(j<(x2-1))){
                    gradnorm=gradNorm(u0[y1*(j+1) + i],u0[y1*(j+1) + i+1],u0[y1*(j-1) + i],u0[y1*(j-1) + i+1],u0[y1*(j) + i],u0[y1*(j) + i+1],h);
                    gs[4*y1*j+4*i+1]=gFunction(gradnorm);//east


                }
                if((j>0)&&(i>0)&&(i<(x1-1))){
                    gradnorm=gradNorm(u0[y1*(j) + i-1],u0[y1*(j-1) + i-1],u0[y1*(j) + i+1],u0[y1*(j-1) + i+1],u0[y1*(j) + i],u0[y1*(j-1) + i],h);
                    gs[(4*y1)*j+4*i+2]=gFunction(gradnorm);//north

                }
                if((j<(x2-1))&&(i>0)&&(i<(x1-1))){
                    gradnorm=gradNorm(u0[y1*(j+1) + i-1],u0[y1*(j) + i-1],u0[y1*(j+1) + i+1],u0[y1*(j) + i+1],u0[y1*(j+1) + i],u0[y1*(j) + i],h);
                    gs[(4*y1)*j+4*i+3]=gFunction(gradnorm);//south

                }

            }

        }

        return gs;
    }
    else
        return u0;

}

QImage* ImageViewer::perona_malikRegSOR(int poc, double h, double tau, double omega){

    ViewerWidget* w = getCurrentViewerWidget();
    w->setUpdatesEnabled(true);
    QImage img1,img = *w->getImage(),*sol;
    uchar* dat0 = img.bits();
    double *u0,*un,*unn,apq = (tau)/(h*h),y=0,*R,RR=0,*gs;
    int row=w->getImage()->bytesPerLine(),x1=w->getImgWidth(),x2=w->getImgHeight(),l = 0,y1=x1,y2=x2;
    bool b = true;
    double tol = 1e-3;

    un = new double[x1*x2];
    unn = new double[x1*x2];
    u0 = new double[x1*x2];
    R = new double[x1*x2];
    gs = new double[4*x1*x2];
    for(int p=0;p<(x1*x2);p++){
        u0[p]=(double)dat0[p];
        un[p]=u0[p];
        unn[p]=0.;
        R[p]=0.;
    }
//    for (int p=0;p<(4*x1*x2);p++) {
//        gs[p]=0.;
//    }

    sol = new QImage[2*poc];
    double suml=0,suml1=0,pocs=0,gpq=0.,gradnorm=0.;

    for(int i = 0; i<2*poc; i++){

        sol[i]=QImage(QSize(x1,x2), QImage::Format_Grayscale8);

    }
    for (int j = 0; j < x2; j++)
    {
        for (int i = 0; i < x1; i++)
        {

            sol[0].bits()[x1*j + i]=static_cast<uchar>(u0[x1*j+i]+0.5);

        }
    }
    for(int m=1; m<poc;m++){
        l = 0;
        b=true;
        gs = linearDiffusionSOR(u0,x1,x2,2,1,0.05,1.1,true);
        while (b==true) {
            RR=0;
            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {


                    suml=0;suml1=0;gpq=0;
                    if(i>0){
                        gpq = gpq + gs[4*x1*j + 4*i + 0];
                        suml1=suml1+apq*gs[4*x1*j + 4*i + 0]*unn[x1*j + i-1];

                    }
                    if(i<(x1-1)){
                        gpq = gpq + gs[4*x1*j + 4*i + 1];
                        suml=suml+apq*gs[4*x1*j + 4*i + 1]*un[x1*j + i+1];
                    }
                    if(j>0){
                        gpq = gpq + gs[4*x1*j + 4*i + 2];
                        suml1=suml1+apq*gs[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];
                    }
                    if(j<(x2-1)){
                        gpq = gpq + gs[4*x1*j + 4*i + 3];
                        suml=suml+apq*gs[4*x1*j + 4*i + 3]*un[x1*(j+1) + i];
                    }

//                    y = (u0[x1*j + i] + suml1 + suml)/(1.0+gpq*apq);
                    y = (u0[x1*j + i] + suml1 + suml)/(1.0+gpq*apq);
                    unn[x1*j + i] = un[x1*j + i] + omega*(y - un[x1*j + i]);
                }


            }

            l++;

            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {
                    //s povodnym gradientom
                    suml=0;suml1=0;gpq=0;
                    if(i>0){
                        gpq = gpq + gs[4*x1*j + 4*i + 0];
                        suml1=suml1+apq*gs[4*x1*j + 4*i + 0]*unn[x1*j + i-1];

                    }
                    if(i<(x1-1)){
                        gpq = gpq + gs[4*x1*j + 4*i + 1];
                        suml1=suml1+apq*gs[4*x1*j + 4*i + 1]*unn[x1*j + i+1];
                    }
                    if(j>0){
                        gpq = gpq + gs[4*x1*j + 4*i + 2];
                        suml1=suml1+apq*gs[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];
                    }
                    if(j<(x2-1)){
                        gpq = gpq + gs[4*x1*j + 4*i + 3];
                        suml1=suml1+apq*gs[4*x1*j + 4*i + 3]*unn[x1*(j+1) + i];
                    }

                    R[x1*j + i] = (1+(gpq*apq))*unn[x1*j + i] - (suml1) - u0[x1*j + i];
                    RR=RR+(R[x1*j + i]*R[x1*j + i]);

                }
            }


            for(int p=0;p<(x1*x2);p++){
                un[p]=unn[p];
                unn[p]=0.;
            }

            if((sqrt(RR)<tol)||(l>500)){
                b=false;
            }

        }


        for (int j = 0; j < x2; j++)
        {
            for (int i = 0; i < x1; i++)
            {
                sol[2*m+0].bits()[x1*j + i]=static_cast<uchar>(un[x1*j+i]+0.5);
                //toto je iba obrazok hodnot fcie g(s)
                sol[2*m+1].bits()[x1*j + i]=static_cast<uchar>(255*(gs[4*x1*j+4*i+0]+gs[4*x1*j+4*i+1]+gs[4*x1*j+4*i+2]+gs[4*x1*j+4*i+3])/4.+0.5);


            }
        }
//        qDebug()<<"l:="<<l<<endl;

        //previerka ci sa zachovava stredna hodnota vsetkych pixlov, mame izolovanu sustavu, cize nic neutieklo
        double mean=0;

        for(int p=0;p<(x1*x2);p++){
            u0[p]=un[p];
            mean=mean+un[p];
        }

        qDebug()<<"meanReg:="<<mean/(x1*x2)<<endl;

    }
    qDebug()<<"m:="<<poc<<endl;

    return sol;


}

double ImageViewer::gRegFunction(double s){

    double eps=10e-4,res=0.;
    //eps je evinsonovva regularizacia, lebo delime normou gradientu preto nam
    //nemoze vyjst nulovy a pridame mini regularizacny parameter aby sme sa
    //tomu vyhli

    res = 1./sqrt(s*s+eps);

    return res;
}

//double* ImageViewer::gRegArray(double *u0,int x1,int x2,double h){

//    double *gs=new double [4*x1*x2],gradnorm=0,*uu0;
//    int y1=x1,y2,n=1;

////    for(int p=0;p<(4*x1*x2);p++){
////        gs[p]=1.;
////    }

//    uu0 = mirror(n,u0,x1,x2);
//    y1 = x1+n*2;
//    y2 = x2+n*2;
//    int z1=x1+n*1;
//    delete u0;
//    u0=new double[y1*y2];

//    for(int p=0;p<(y1*y2);p++){
//        u0[p]=uu0[p];
//    }

//    for (int j = 1; j < y2-1; j++)
//    {
//        for (int i = 1; i < y1-1; i++)
//        {

////            gradnorm=gradNorm(u0[y1*(j+1) + i-1],u0[y1*(j+1) + i],u0[y1*(j-1) + i-1],u0[y1*(j-1) + i],u0[y1*(j) + i-1],u0[y1*(j) + i],h);
////            gs[4*x1*(j-1)+4*(i-1)+0]=gRegFunction(gradnorm);//west

////            gradnorm=gradNorm(u0[y1*(j+1) + i],u0[y1*(j+1) + i+1],u0[y1*(j-1) + i],u0[y1*(j-1) + i+1],u0[y1*(j) + i],u0[y1*(j) + i+1],h);
////            gs[4*x1*(j-1)+4*(i-1)+1]=gRegFunction(gradnorm);//east

////            gradnorm=gradNorm(u0[y1*(j) + i-1],u0[y1*(j-1) + i-1],u0[y1*(j) + i+1],u0[y1*(j-1) + i+1],u0[y1*(j) + i],u0[y1*(j-1) + i],h);
////            gs[4*x1*(j-1)+4*(i-1)+2]=gRegFunction(gradnorm);//north

////            gradnorm=gradNorm(u0[y1*(j+1) + i-1],u0[y1*(j) + i-1],u0[y1*(j+1) + i+1],u0[y1*(j) + i+1],u0[y1*(j+1) + i],u0[y1*(j) + i],h);
////            gs[4*x1*(j-1)+4*(i-1)+3]=gRegFunction(gradnorm);//south

//            gradnorm=gradNorm(u0[z1*(j+1) + i-1],u0[z1*(j+1) + i],u0[z1*(j-1) + i-1],u0[z1*(j-1) + i],u0[z1*(j) + i-1],u0[z1*(j) + i],h);
//            gs[4*x1*(j-1)+4*(i-1)+0]=gRegFunction(gradnorm);//west

//            gradnorm=gradNorm(u0[z1*(j+1) + i],u0[z1*(j+1) + i+1],u0[z1*(j-1) + i],u0[z1*(j-1) + i+1],u0[z1*(j) + i],u0[z1*(j) + i+1],h);
//            gs[4*x1*(j-1)+4*(i-1)+1]=gRegFunction(gradnorm);//east

//            gradnorm=gradNorm(u0[z1*(j) + i-1],u0[z1*(j-1) + i-1],u0[z1*(j) + i+1],u0[z1*(j-1) + i+1],u0[z1*(j) + i],u0[z1*(j-1) + i],h);
//            gs[4*x1*(j-1)+4*(i-1)+2]=gRegFunction(gradnorm);//north

//            gradnorm=gradNorm(u0[z1*(j+1) + i-1],u0[z1*(j) + i-1],u0[z1*(j+1) + i+1],u0[z1*(j) + i+1],u0[z1*(j+1) + i],u0[z1*(j) + i],h);
//            gs[4*x1*(j-1)+4*(i-1)+3]=gRegFunction(gradnorm);//south



//        }
//    }

////    for(int p=0;p<(4*x1*x2);p++)
////        qDebug()<<"gs:="<<p<<" "<<gs[p]<<endl;


//    return gs;


//}
double* ImageViewer::gRegArray(double *u0,int x1,int x2,double h){

    double *gs=new double [4*x1*x2],gradnorm=0;
    int y1=x1;

    for(int p=0;p<(4*x1*x2);p++){
        gs[p]=125.;
    }

    for (int j = 0; j < x2; j++)
    {
        for (int i = 0; i < x1; i++)
        {

            if((i>0)&&(j>0)&&(j<(x2-1))){
                gradnorm=gradNorm(u0[y1*(j+1) + i-1],u0[y1*(j+1) + i],u0[y1*(j-1) + i-1],u0[y1*(j-1) + i],u0[y1*(j) + i-1],u0[y1*(j) + i],h);
                gs[4*y1*j+4*i+0]=gRegFunction(gradnorm);//west

            }
            else if(i==0&&j==0){
                gradnorm=gradNorm(0,u0[y1*(j+1) + i],0,0,0,u0[y1*(j) + i],h);
                gs[4*y1*j+4*i+0]=gRegFunction(gradnorm);//west

            }
            else if(i==0&&j<x2-1){
                gradnorm=gradNorm(0,u0[y1*(j+1) + i],0,u0[y1*(j-1) + i],0,u0[y1*(j) + i],h);
                gs[4*y1*j+4*i+0]=gRegFunction(gradnorm);//west

            }
            else if(i==0&&j==x2-1){
                gradnorm=gradNorm(0,0,0,u0[y1*(j-1) + i],0,u0[y1*(j) + i],h);
                gs[4*y1*j+4*i+0]=gRegFunction(gradnorm);//west

            }
            if((i<(x1-1))&&(j>0)&&(j<(x2-1))){
                gradnorm=gradNorm(u0[y1*(j+1) + i],u0[y1*(j+1) + i+1],u0[y1*(j-1) + i],u0[y1*(j-1) + i+1],u0[y1*(j) + i],u0[y1*(j) + i+1],h);
                gs[4*y1*j+4*i+1]=gRegFunction(gradnorm);//east
            }
            else if(i==x1-1&&j==0){
                gradnorm=gradNorm(u0[y1*(j+1) + i],0,0,0,0,u0[y1*(j) + i],h);
                gs[4*y1*j+4*i+1]=gRegFunction(gradnorm);//east

            }
            else if(i==(x1-1)&&j<x2-1){
                gradnorm=gradNorm(u0[y1*(j+1) + i],0,u0[y1*(j-1) + i],0,u0[y1*(j) + i],0,h);
                gs[4*y1*j+4*i+1]=gRegFunction(gradnorm);//east

            }
            else if(i==x1-1&&j==x2-1){
                gradnorm=gradNorm(0,0,u0[y1*(j-1) + i],0,u0[y1*(j) + i],0,h);
                gs[4*y1*j+4*i+1]=gRegFunction(gradnorm);//east

            }
            if((j>0)&&(i>0)&&(i<(x1-1))){
                gradnorm=gradNorm(u0[y1*(j) + i-1],u0[y1*(j-1) + i-1],u0[y1*(j) + i+1],u0[y1*(j-1) + i+1],u0[y1*(j) + i],u0[y1*(j-1) + i],h);
                gs[(4*y1)*j+4*i+2]=gRegFunction(gradnorm);//north

            }
            else if(j==0&&i==0){
                gradnorm=gradNorm(0,u0[y1*(j-1) + i-1],u0[y1*(j) + i+1],0,u0[y1*(j) + i],0,h);
                gs[4*y1*j+4*i+2]=gRegFunction(gradnorm);//north

            }
            else if(j==0&&i<x1-1){
                gradnorm=gradNorm(u0[y1*(j) + i-1],0,u0[y1*(j) + i+1],0,u0[y1*(j) + i],0,h);
                gs[4*y1*j+4*i+2]=gRegFunction(gradnorm);//north

            }
            else if(j==0&&i==x1-1){
                gradnorm=gradNorm(u0[y1*(j) + i-1],0,0,0,u0[y1*(j) + i],0,h);
                gs[4*y1*j+4*i+2]=gRegFunction(gradnorm);//north

            }
            if((j<(x2-1))&&(i>0)&&(i<(x1-1))){
                gradnorm=gradNorm(u0[y1*(j+1) + i-1],u0[y1*(j) + i-1],u0[y1*(j+1) + i+1],u0[y1*(j) + i+1],u0[y1*(j+1) + i],u0[y1*(j) + i],h);
                gs[(4*y1)*j+4*i+3]=gRegFunction(gradnorm);//south
            }
            else if(j==(x2-1)&&i==0){
                gradnorm=gradNorm(0,0,0,u0[y1*(j) + i+1],0,u0[y1*(j) + i],h);
                gs[4*y1*j+4*i+3]=gRegFunction(gradnorm);//south

            }
            else if(j==(x2-1)&&i<x1-1){
                gradnorm=gradNorm(0,u0[y1*(j) + i-1],0,u0[y1*(j) + i+1],0,u0[y1*(j) + i],h);
                gs[4*y1*j+4*i+3]=gRegFunction(gradnorm);//south

            }
            else if(j==(x2-1)&&i==x1-1){
                gradnorm=gradNorm(0,u0[y1*(j) + i-1],0,0,0,u0[y1*(j) + i],h);
                gs[4*y1*j+4*i+3]=gRegFunction(gradnorm);//south

            }


        }
    }

//    for(int p=0;p<(4*x1*x2);p++)
//        qDebug()<<"gs1:="<<p<<" "<<gs[p]<<endl;


    return gs;

}

QImage* ImageViewer::curvature_filterSOR(int poc, double h, double tau, double omega){

    ViewerWidget* w = getCurrentViewerWidget();
    w->setUpdatesEnabled(true);
    QImage img1,img = *w->getImage(),*sol;
    uchar* dat0 = img.bits();
    double *u0,*un,*unn,y=0,*R,RR=0,*gs;
    int row=w->getImage()->bytesPerLine(),x1=w->getImgWidth(),x2=w->getImgHeight(),l = 0,y1=x1,y2=x2;
    bool b = true;
    double tol = 1e-8,mean=0.;

    un = new double[x1*x2];
    unn = new double[x1*x2];
    u0 = new double[x1*x2];
    R = new double[x1*x2];
    gs = new double[4*x1*x2];
    for(int p=0;p<(x1*x2);p++){
        u0[p]=(double)dat0[p];
        un[p]=u0[p];
        mean=mean+un[p];

        unn[p]=0.;
        R[p]=0.;
    }

    qDebug()<<"mean:="<<mean/(x1*x2)<<endl;

    sol = new QImage[2*poc];
    double apq0=0,apq1=0,gradvol=0.;

    for(int i = 0; i<2*poc; i++){

        sol[i]=QImage(QSize(x1,x2), QImage::Format_Grayscale8);

    }
    for (int j = 0; j < x2; j++)
    {
        for (int i = 0; i < x1; i++)
        {

            sol[0].bits()[x1*j + i]=static_cast<uchar>(u0[x1*j+i]+0.5);

        }
    }

    for(int m=1; m<poc;m++){
        l = 0;
        b=true;
        gs = gRegArray(u0,x1,x2,h);
        while (b==true) {
            RR=0;
            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {

                    apq0=0.;apq1=0.;gradvol=0.;

                    if(i>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 0];
                        apq0=apq0+gs[4*x1*j + 4*i + 0];
                        apq1=apq1+gs[4*x1*j + 4*i + 0]*unn[x1*j + i-1];

                    }
                    if(i<(x1-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 1];
                        apq0=apq0+gs[4*x1*j + 4*i + 1];
                        apq1=apq1+gs[4*x1*j + 4*i + 1]*un[x1*j + i+1];

                    }
                    if(j>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 2];
                        apq0=apq0+gs[4*x1*j + 4*i + 2];
                        apq1=apq1+gs[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];

                    }
                    if(j<(x2-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 3];
                        apq0=apq0+gs[4*x1*j + 4*i + 3];
                        apq1=apq1+gs[4*x1*j + 4*i + 3]*un[x1*(j+1) + i];

                    }

                    //if(gradvol!=0.0)
//                    gradvol = (1./gradvol)/4.;
                    gradvol = 4./gradvol;

                    qDebug()<<l<<" "<<x1*j + i<<" gradvol:="<<gradvol<<endl;
                    apq0 = (tau/(h*h))*gradvol*apq0;

                    apq1 = (tau/(h*h))*gradvol*apq1;

                    y = (u0[x1*j + i] + apq1)/(1.0+apq0);
                    unn[x1*j + i] = un[x1*j + i] + omega*(y - un[x1*j + i]);

                }


            }

            l++;

            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {

                    apq0=0.;apq1=0.;gradvol=0.;

                    if(i>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 0];
                        apq0=apq0+gs[4*x1*j + 4*i + 0];
                        apq1=apq1+gs[4*x1*j + 4*i + 0]*unn[x1*j + i-1];


                    }
                    if(i<(x1-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 1];
                        apq0=apq0+gs[4*x1*j + 4*i + 1];
                        apq1=apq1+gs[4*x1*j + 4*i + 1]*unn[x1*j + i+1];
                    }
                    if(j>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 2];
                        apq0=apq0+gs[4*x1*j + 4*i + 2];
                        apq1=apq1+gs[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];
                    }
                    if(j<(x2-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 3];
                        apq0=apq0+gs[4*x1*j + 4*i + 3];
                        apq1=apq1+gs[4*x1*j + 4*i + 3]*unn[x1*(j+1) + i];
                    }

                    //if(gradvol!=0.0)
//                    gradvol = (1./gradvol)/4.;
                    gradvol = 4./gradvol;

                    apq0 = (tau/(h*h))*gradvol*apq0;
                    apq1 = (tau/(h*h))*gradvol*apq1;

                    R[x1*j + i] = (1+(apq0))*unn[x1*j + i] - (apq1) - u0[x1*j + i];
                    RR=RR+(R[x1*j + i]*R[x1*j + i]);

                }
            }


            for(int p=0;p<(x1*x2);p++){
                un[p]=unn[p];
                unn[p]=0.;
            }

            if((sqrt(RR)<tol)||(l>500)){
                b=false;
            }

        }


        for (int j = 0; j < x2; j++)
        {
            for (int i = 0; i < x1; i++)
            {
                sol[2*m+0].bits()[x1*j + i]=static_cast<uchar>(un[x1*j+i]+0.5);
                //toto je iba obrazok hodnot fcie g(s)
                sol[2*m+1].bits()[x1*j + i]=static_cast<uchar>(255*(gs[4*x1*j+4*i+0]+gs[4*x1*j+4*i+1]+gs[4*x1*j+4*i+2]+gs[4*x1*j+4*i+3])/4.+0.5);


            }
        }
        qDebug()<<"l:="<<l<<endl;
        for(int p=0;p<(x1*x2);p++){
            u0[p]=un[p];
        }
        //previerka ci sa zachovava stredna hodnota vsetkych pixlov, mame izolovanu sustavu, cize nic neutieklo


    }
    mean=0;

    for(int p=0;p<(x1*x2);p++){
        mean=mean+u0[p];
    }

    qDebug()<<"meanMCF:="<<mean/(x1*x2)<<endl;
    qDebug()<<"m:="<<poc<<endl;

    return sol;


}

//geodesic MCF

QImage* ImageViewer::geodesic_curvature_filterSOR(int poc, double h, double tau, double omega){

    ViewerWidget* w = getCurrentViewerWidget();
    w->setUpdatesEnabled(true);
    QImage img1,img = *w->getImage(),*sol;
    uchar* dat0 = img.bits();
    double *u0,*un,*unn,y=0,*R,RR=0,*gs,*uheat,*gsConv;
    int row=w->getImage()->bytesPerLine(),x1=w->getImgWidth(),x2=w->getImgHeight(),l = 0,y1=x1,y2=x2;
    bool b = true;
    double tol = 1e-8,mean=0.;

    un = new double[x1*x2];
    unn = new double[x1*x2];
    u0 = new double[x1*x2];
    R = new double[x1*x2];
    gs = new double[4*x1*x2];
    for(int p=0;p<(x1*x2);p++){
        u0[p]=(double)dat0[p];
        un[p]=u0[p];
        mean=mean+un[p];

        unn[p]=0.;
        R[p]=0.;
    }

    qDebug()<<"mean:="<<mean/(x1*x2)<<endl;

    sol = new QImage[2*poc];
    double apq0=0,apq1=0,gradvol=0.;

    for(int i = 0; i<2*poc; i++){

        sol[i]=QImage(QSize(x1,x2), QImage::Format_Grayscale8);

    }
    for (int j = 0; j < x2; j++)
    {
        for (int i = 0; i < x1; i++)
        {

            sol[0].bits()[x1*j + i]=static_cast<uchar>(u0[x1*j+i]+0.5);

        }
    }

    for(int m=1; m<poc;m++){
        l = 0;
        b=true;
        gs = gRegArray(u0,x1,x2,h);
        gsConv = linearDiffusionSOR(u0,x1,x2,2,1,0.05,1.1,true);
        while (b==true) {
            RR=0;
            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {

                    apq0=0.;apq1=0.;gradvol=0.;

                    if(i>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 0];
                        apq0=apq0+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0];
                        apq1=apq1+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0]*unn[x1*j + i-1];

                    }
                    if(i<(x1-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 1];
                        apq0=apq0+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1];
                        apq1=apq1+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1]*un[x1*j + i+1];

                    }
                    if(j>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 2];
                        apq0=apq0+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2];
                        apq1=apq1+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];

                    }
                    if(j<(x2-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 3];
                        apq0=apq0+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3];
                        apq1=apq1+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3]*un[x1*(j+1) + i];

                    }

                    //if(gradvol!=0.0)
//                    gradvol = (1./gradvol)/4.;
                    gradvol = 4./gradvol;

                   // qDebug()<<"gradvol:="<<gradvol<<endl;
                    apq0 = (tau/(h*h))*gradvol*apq0;

                    apq1 = (tau/(h*h))*gradvol*apq1;

                    y = (u0[x1*j + i] + apq1)/(1.0+apq0);
                    unn[x1*j + i] = un[x1*j + i] + omega*(y - un[x1*j + i]);

                }


            }

            l++;

            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {

                    apq0=0.;apq1=0.;gradvol=0.;

                    if(i>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 0];
                        apq0=apq0+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0];
                        apq1=apq1+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0]*unn[x1*j + i-1];


                    }
                    if(i<(x1-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 1];
                        apq0=apq0+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1];
                        apq1=apq1+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1]*unn[x1*j + i+1];
                    }
                    if(j>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 2];
                        apq0=apq0+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2];
                        apq1=apq1+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];
                    }
                    if(j<(x2-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 3];
                        apq0=apq0+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3];
                        apq1=apq1+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3]*unn[x1*(j+1) + i];
                    }

                    //if(gradvol!=0.0)
//                    gradvol = (1./gradvol)/4.;
                    gradvol = 4./gradvol;

                    apq0 = (tau/(h*h))*gradvol*apq0;
                    apq1 = (tau/(h*h))*gradvol*apq1;

                    R[x1*j + i] = (1+(apq0))*unn[x1*j + i] - (apq1) - u0[x1*j + i];
                    RR=RR+(R[x1*j + i]*R[x1*j + i]);

                }
            }


            for(int p=0;p<(x1*x2);p++){
                un[p]=unn[p];
                unn[p]=0.;
            }

            if((sqrt(RR)<tol)||(l>500)){
                b=false;
            }

        }


        for (int j = 0; j < x2; j++)
        {
            for (int i = 0; i < x1; i++)
            {
                sol[2*m+0].bits()[x1*j + i]=static_cast<uchar>(un[x1*j+i]+0.5);
                //toto je iba obrazok hodnot fcie g(s)
                sol[2*m+1].bits()[x1*j + i]=static_cast<uchar>(255*(gsConv[4*x1*j+4*i+0]+gsConv[4*x1*j+4*i+1]+gsConv[4*x1*j+4*i+2]+gsConv[4*x1*j+4*i+3])/4.+0.5);


            }
        }
        qDebug()<<"l:="<<l<<endl;
        for(int p=0;p<(x1*x2);p++){
            u0[p]=un[p];
        }
        //previerka ci sa zachovava stredna hodnota vsetkych pixlov, mame izolovanu sustavu, cize nic neutieklo
        mean=0;

        for(int p=0;p<(x1*x2);p++){
            mean=mean+u0[p];
        }

        qDebug()<<"meanGMCF:="<<mean/(x1*x2)<<endl;

    }
    mean=0;

    for(int p=0;p<(x1*x2);p++){
        mean=mean+u0[p];
    }

    qDebug()<<"meanGMCF:="<<mean/(x1*x2)<<endl;
    qDebug()<<"m:="<<poc<<endl;

    return sol;


}

double* ImageViewer::loadDF(QString filename){

    QFile file(filename);
    int n,i=0;
    double *df;

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return 0;

    QTextStream in(&file);

    n = in.readLine().toInt();
    df = new double[(n*n)];

    in.readLine();

    while (!in.atEnd()) {
        QString line = in.readLine();
        df[i] = line.toDouble();
        i++;
    }


    return df;

}


void ImageViewer::ZeroLineEvolutionSOR(int poc, double h, double tau, double omega,QString filename){

    double *u0,*un,*unn,y=0,*R,RR=0,*gs,*uheat,*gsConv;
    int l = 0,x1=200,x2=200,y1=x1,y2=x2;
    bool b = true;
    double tol = 1e-8,mean=0.;
//    QString filename1 = "/Users/leaandshoshana/Desktop/Bakalarka/zeroevolution1.txt";
//    QString filename1 = "/Users/leaandshoshana/Desktop/Bakalarka/zeroevolutionHEART1.txt";
//    QString filename1 = "/Users/leaandshoshana/Desktop/Bakalarka/zeroDistFunSO1.txt";
    QString filename1 = "/Users/leaandshoshana/Desktop/Bakalarka/zeroDistFunPaula.txt";

    QFile file(filename1);
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        stream << 200 << endl;
        stream << 200 << endl;
        stream << poc << endl;

    }
    else
        qDebug()<<"File Not Open"<<endl;

    un = new double[x1*x2];
    unn = new double[x1*x2];
    u0 = new double[x1*x2];
    R = new double[x1*x2];
    gs = new double[4*x1*x2];
    u0=loadDF(filename);
    for(int p=0;p<(x1*x2);p++){
        un[p]=u0[p];
        mean=mean+un[p];
        unn[p]=0.;
        R[p]=0.;
    }

    qDebug()<<"meanZE:="<<mean/(x1*x2)<<endl;

    double apq0=0,apq1=0,gradvol=0.;

    for(int m=0; m<poc;m++){
        l = 0;
        b=true;
        gs = gRegArray(u0,x1,x2,h);
        gsConv = linearDiffusionSOR(u0,x1,x2,2,1,0.05,1.1,true);
        while (b==true) {
            RR=0;
            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {

                    apq0=0.;apq1=0.;gradvol=0.;

                    if(i>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 0];
                        apq0=apq0+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0];
                        apq1=apq1+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0]*unn[x1*j + i-1];

                    }
                    if(i<(x1-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 1];
                        apq0=apq0+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1];
                        apq1=apq1+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1]*un[x1*j + i+1];

                    }
                    if(j>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 2];
                        apq0=apq0+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2];
                        apq1=apq1+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];

                    }
                    if(j<(x2-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 3];
                        apq0=apq0+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3];
                        apq1=apq1+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3]*un[x1*(j+1) + i];

                    }

                    //if(gradvol!=0.0)
//                    gradvol = (1./gradvol)/4.;
                    gradvol = 4./gradvol;

                   // qDebug()<<"gradvol:="<<gradvol<<endl;
                    apq0 = (tau/(h*h))*gradvol*apq0;

                    apq1 = (tau/(h*h))*gradvol*apq1;

                    y = (u0[x1*j + i] + apq1)/(1.0+apq0);
                    unn[x1*j + i] = un[x1*j + i] + omega*(y - un[x1*j + i]);

                }


            }

            l++;

            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {

                    apq0=0.;apq1=0.;gradvol=0.;

                    if(i>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 0];
                        apq0=apq0+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0];
                        apq1=apq1+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0]*unn[x1*j + i-1];


                    }
                    if(i<(x1-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 1];
                        apq0=apq0+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1];
                        apq1=apq1+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1]*unn[x1*j + i+1];
                    }
                    if(j>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 2];
                        apq0=apq0+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2];
                        apq1=apq1+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];
                    }
                    if(j<(x2-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 3];
                        apq0=apq0+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3];
                        apq1=apq1+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3]*unn[x1*(j+1) + i];
                    }

                    //if(gradvol!=0.0)
//                    gradvol = (1./gradvol)/4.;
                    gradvol = 4./gradvol;

                    apq0 = (tau/(h*h))*gradvol*apq0;
                    apq1 = (tau/(h*h))*gradvol*apq1;

                    R[x1*j + i] = (1+(apq0))*unn[x1*j + i] - (apq1) - u0[x1*j + i];
                    RR=RR+(R[x1*j + i]*R[x1*j + i]);

                }
            }


            for(int p=0;p<(x1*x2);p++){
                un[p]=unn[p];
                unn[p]=0.;
            }

            if((sqrt(RR)<tol)||(l>500)){
                b=false;
            }

        }

        //qDebug()<<"l:="<<l<<endl;
        for(int p=0;p<(x1*x2);p++){
            u0[p]=un[p];
            QTextStream stream(&file);
            stream << u0[p] << endl;
        }
        //previerka ci sa zachovava stredna hodnota vsetkych pixlov, mame izolovanu sustavu, cize nic neutieklo
        mean=0;

        for(int p=0;p<(x1*x2);p++){
            mean=mean+u0[p];
        }

        //qDebug()<<"meanZE:="<<mean/(x1*x2)<<endl;

    }

    qDebug()<<"m:="<<poc<<endl;


}
//QImage* ImageViewer::curvature_filterSOR(int poc, double h, double tau, double omega){

//    ViewerWidget* w = getCurrentViewerWidget();
//    w->setUpdatesEnabled(true);
//    QImage img1,img = *w->getImage(),*sol;
//    uchar* dat0 = img.bits();
//    double *u0,*un,*unn,y=0,*R,RR=0,*gs;
//    int row=w->getImage()->bytesPerLine(),x1=w->getImgWidth(),x2=w->getImgHeight(),l = 0,y1=x1,y2=x2;
//    bool b = true;
//    double tol = 1e-3;

//    un = new double[x1*x2];
//    unn = new double[x1*x2];
//    u0 = new double[x1*x2];
//    R = new double[x1*x2];
//    gs = new double[4*x1*x2];
//    for(int p=0;p<(x1*x2);p++){
//        u0[p]=(double)dat0[p];
//        un[p]=u0[p];
//        unn[p]=0.;
//        R[p]=0.;
//    }

//    sol = new QImage[2*poc];
//    double apq0=0,apq1=0,gradvol=0.;

//    for(int i = 0; i<2*poc; i++){

//        sol[i]=QImage(QSize(x1,x2), QImage::Format_Grayscale8);

//    }
//    gs = gRegArray(u0,x1,x2,h);

//    for (int j = 0; j < x2; j++)
//    {
//        for (int i = 0; i < x1; i++)
//        {

//            sol[0].bits()[x1*j + i]=static_cast<uchar>(u0[x1*j+i]+0.5);
//            sol[1].bits()[x1*j + i]=static_cast<uchar>(255*(gs[4*x1*j+4*i+0]+gs[4*x1*j+4*i+1]+gs[4*x1*j+4*i+2]+gs[4*x1*j+4*i+3])/4.+0.5);


//        }
//    }

//    for(int m=1; m<poc;m++){
//        l = 0;
//        b=true;
//        while (b==true) {
//            RR=0;
//            for (int j = 0; j < x2; j++)
//            {
//                for (int i = 0; i < x1; i++)
//                {

//                    apq0=0.;apq1=0.;gradvol=0.;

//                    if(i>0){
//                        gradvol = gradvol + gs[4*x1*j + 4*i + 0]*gs[4*x1*j + 4*i + 0];
//                        apq0=apq0+gs[4*x1*j + 4*i + 0];
//                        apq1=apq1+gs[4*x1*j + 4*i + 0]*unn[x1*j + i-1];

//                    }
//                    if(i<(x1-1)){
//                        gradvol = gradvol + gs[4*x1*j + 4*i + 1]*gs[4*x1*j + 4*i + 1];
//                        apq0=apq0+gs[4*x1*j + 4*i + 1];
//                        apq1=apq1+gs[4*x1*j + 4*i + 1]*un[x1*j + i+1];

//                    }
//                    if(j>0){
//                        gradvol = gradvol + gs[4*x1*j + 4*i + 2]*gs[4*x1*j + 4*i + 2];
//                        apq0=apq0+gs[4*x1*j + 4*i + 2];
//                        apq1=apq1+gs[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];

//                    }
//                    if(j<(x2-1)){
//                        gradvol = gradvol + gs[4*x1*j + 4*i + 3]*gs[4*x1*j + 4*i + 3];
//                        apq0=apq0+gs[4*x1*j + 4*i + 3];
//                        apq1=apq1+gs[4*x1*j + 4*i + 3]*un[x1*(j+1) + i];

//                    }

//                    //if(gradvol!=0.0)
//                    gradvol = 4./gradvol;
//                   // qDebug()<<"gradvol:="<<gradvol<<endl;
//                    apq0 = (tau/(h*h))*gradvol*apq0;

//                    apq1 = (tau/(h*h))*gradvol*apq1;

//                    y = (u0[x1*j + i] + apq1)/(1.0+apq0);
//                    unn[x1*j + i] = un[x1*j + i] + omega*(y - un[x1*j + i]);

//                }


//            }

//            l++;

//            for (int j = 0; j < x2; j++)
//            {
//                for (int i = 0; i < x1; i++)
//                {

//                    apq0=0.;apq1=0.;gradvol=0.;

//                    if(i>0){
//                        gradvol = gradvol + gs[4*x1*j + 4*i + 0]*gs[4*x1*j + 4*i + 0];
//                        apq0=apq0+gs[4*x1*j + 4*i + 0];
//                        apq1=apq1+gs[4*x1*j + 4*i + 0]*unn[x1*j + i-1];


//                    }
//                    if(i<(x1-1)){
//                        gradvol = gradvol + gs[4*x1*j + 4*i + 1]*gs[4*x1*j + 4*i + 1];
//                        apq0=apq0+gs[4*x1*j + 4*i + 1];
//                        apq1=apq1+gs[4*x1*j + 4*i + 1]*unn[x1*j + i+1];
//                    }
//                    if(j>0){
//                        gradvol = gradvol + gs[4*x1*j + 4*i + 2]*gs[4*x1*j + 4*i + 2];
//                        apq0=apq0+gs[4*x1*j + 4*i + 2];
//                        apq1=apq1+gs[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];
//                    }
//                    if(j<(x2-1)){
//                        gradvol = gradvol + gs[4*x1*j + 4*i + 3]*gs[4*x1*j + 4*i + 3];
//                        apq0=apq0+gs[4*x1*j + 4*i + 3];
//                        apq1=apq1+gs[4*x1*j + 4*i + 3]*unn[x1*(j+1) + i];
//                    }

//                    //if(gradvol!=0.0)
//                    gradvol = 4./gradvol;
//                    apq0 = (tau/(h*h))*gradvol*apq0;
//                    apq1 = (tau/(h*h))*gradvol*apq1;

//                    R[x1*j + i] = (1+(apq0))*unn[x1*j + i] - (apq1) - u0[x1*j + i];
//                    RR=RR+(R[x1*j + i]*R[x1*j + i]);

//                }
//            }


//            for(int p=0;p<(x1*x2);p++){
//                un[p]=unn[p];
//                unn[p]=0.;
//            }

//            if((sqrt(RR)<tol)||(l>500)){
//                b=false;
//            }

//        }

//        gs = gRegArray(un,x1,x2,h);

//        for (int j = 0; j < x2; j++)
//        {
//            for (int i = 0; i < x1; i++)
//            {
//                sol[2*m+0].bits()[x1*j + i]=static_cast<uchar>(un[x1*j+i]+0.5);
//                //toto je iba obrazok hodnot fcie g(s)
//                sol[2*m+1].bits()[x1*j + i]=static_cast<uchar>(255*(gs[4*x1*j+4*i+0]+gs[4*x1*j+4*i+1]+gs[4*x1*j+4*i+2]+gs[4*x1*j+4*i+3])/4.+0.5);


//            }
//        }
//        qDebug()<<"l:="<<l<<endl;

//        //previerka ci sa zachovava stredna hodnota vsetkych pixlov, mame izolovanu sustavu, cize nic neutieklo
////        double mean=0;

//        for(int p=0;p<(x1*x2);p++){
//            u0[p]=un[p];
//            //mean=mean+un[p];
//        }

////        qDebug()<<"mean:="<<mean/(x1*x2)<<endl;

//    }
//    qDebug()<<"m:="<<poc<<endl;

//    return sol;


//}


QImage* ImageViewer::Segmentation_SUBSURF_Bernsen(int poc, double h, double tau, double omega,QString filenameLSF,QString filenameOI){

    double *u0,*un,*unn,y=0,*R,RR=0,*gs,*uheat,*gsConv,*I;
    int x1=80,x2=80,l = 0,y1=x1,y2=x2;
    bool b = true;
    double tol = 1e-8,mean=0.;


    un = new double[x1*x2];
    unn = new double[x1*x2];
    I = new double[x1*x2];
    u0 = new double[x1*x2];
    R = new double[x1*x2];
    gs = new double[4*x1*x2];

    u0 = loadDF(filenameLSF);
    I = loadDF(filenameOI);

    QString filename1 = "/Users/leaandshoshana/Desktop/Bakalarka/Bernsen1.txt";
//    QString filename1 = "/Users/leaandshoshana/Desktop/Bakalarka/Circle1.txt";
    //QString filename1 = "/Users/leaandshoshana/Desktop/Bakalarka/Square1.txt";

    QFile file(filename1);
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        stream << x1 << endl;
        stream << x2 << endl;
        stream << poc << endl;

    }
    else
        qDebug()<<"File Not Open"<<endl;

    for(int p=0;p<(x1*x2);p++){

        u0[p] = 255.0*u0[p];//just for cervik but not suree 100%
        un[p]=u0[p];
        //I[p]=255.0*I[p];//just for circle and square
        mean=mean+un[p];
        unn[p]=0.;
        R[p]=0.;
    }

    qDebug()<<"mean:="<<mean/(x1*x2)<<endl;
    double apq0=0,apq1=0,gradvol=0.;


    for(int m=1; m<poc;m++){
        l = 0;
        b=true;
        gs = gRegArray(u0,x1,x2,h);
        gsConv = linearDiffusionSOR(I,x1,x2,2,1,0.05,1.1,true);
        while (b==true) {
            RR=0;
            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {

                    apq0=0.;apq1=0.;gradvol=0.;

                    if(i>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 0];
                        apq0=apq0+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0];
                        apq1=apq1+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0]*unn[x1*j + i-1];

                    }
                    if(i<(x1-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 1];
                        apq0=apq0+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1];
                        apq1=apq1+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1]*un[x1*j + i+1];

                    }
                    if(j>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 2];
                        apq0=apq0+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2];
                        apq1=apq1+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];

                    }
                    if(j<(x2-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 3];
                        apq0=apq0+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3];
                        apq1=apq1+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3]*un[x1*(j+1) + i];

                    }

                    //if(gradvol!=0.0)
//                    gradvol = (1./gradvol)/4.;
                    gradvol = 4./gradvol;

                   // qDebug()<<"gradvol:="<<gradvol<<endl;
                    apq0 = (tau/(h*h))*gradvol*apq0;

                    apq1 = (tau/(h*h))*gradvol*apq1;

                    y = (u0[x1*j + i] + apq1)/(1.0+apq0);
                    unn[x1*j + i] = un[x1*j + i] + omega*(y - un[x1*j + i]);

                }


            }

            l++;

            for (int j = 0; j < x2; j++)
            {
                for (int i = 0; i < x1; i++)
                {

                    apq0=0.;apq1=0.;gradvol=0.;

                    if(i>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 0];
                        apq0=apq0+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0];
                        apq1=apq1+gs[4*x1*j + 4*i + 0]*gsConv[4*x1*j + 4*i + 0]*unn[x1*j + i-1];


                    }
                    if(i<(x1-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 1];
                        apq0=apq0+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1];
                        apq1=apq1+gs[4*x1*j + 4*i + 1]*gsConv[4*x1*j + 4*i + 1]*unn[x1*j + i+1];
                    }
                    if(j>0){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 2];
                        apq0=apq0+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2];
                        apq1=apq1+gs[4*x1*j + 4*i + 2]*gsConv[4*x1*j + 4*i + 2]*unn[x1*(j-1) + i];
                    }
                    if(j<(x2-1)){
                        gradvol = gradvol + gs[4*x1*j + 4*i + 3];
                        apq0=apq0+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3];
                        apq1=apq1+gs[4*x1*j + 4*i + 3]*gsConv[4*x1*j + 4*i + 3]*unn[x1*(j+1) + i];
                    }

                    //if(gradvol!=0.0)
//                    gradvol = (1./gradvol)/4.;
                    gradvol = 4./gradvol;

                    apq0 = (tau/(h*h))*gradvol*apq0;
                    apq1 = (tau/(h*h))*gradvol*apq1;

                    R[x1*j + i] = (1+(apq0))*unn[x1*j + i] - (apq1) - u0[x1*j + i];
                    RR=RR+(R[x1*j + i]*R[x1*j + i]);

                }
            }


            for(int p=0;p<(x1*x2);p++){
                un[p]=unn[p];
                unn[p]=0.;
            }

            if((sqrt(RR)<tol)||(l>500)){
                b=false;
            }

        }

        for(int p=0;p<(x1*x2);p++){
            u0[p]=un[p];
            QTextStream stream(&file);
            stream << u0[p] << endl;
        }


        qDebug()<<"l:="<<l<<endl;

        //previerka ci sa zachovava stredna hodnota vsetkych pixlov, mame izolovanu sustavu, cize nic neutieklo
//        mean=0;

//        for(int p=0;p<(x1*x2);p++){
//            mean=mean+u0[p];
//        }

//        qDebug()<<"meanSUBSURF:="<<mean/(x1*x2)<<endl;

    }
    mean=0;

    for(int p=0;p<(x1*x2);p++){
        mean=mean+u0[p];
    }

    qDebug()<<"meanSUBSURF:="<<mean/(x1*x2)<<endl;
    qDebug()<<"m:="<<poc<<endl;

    return sol;


}




//Slots

//Tabs slots
void ImageViewer::on_tabWidget_tabCloseRequested(int tabId)
{
    ViewerWidget* vW = getViewerWidget(tabId);
    vW->~ViewerWidget();
    ui->tabWidget->removeTab(tabId);
}
void ImageViewer::on_actionRename_triggered()
{
    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }
    ViewerWidget* w = getCurrentViewerWidget();
    bool ok;
    QString text = QInputDialog::getText(this, QString("Rename"), tr("Image name:"), QLineEdit::Normal, w->getName(), &ok);
    if (ok && !text.trimmed().isEmpty())
    {
        w->setName(text);
        ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), text);
    }
}

//Image slots
void ImageViewer::on_actionNew_triggered()
{
    newImgDialog = new NewImageDialog(this);
    connect(newImgDialog, SIGNAL(accepted()), this, SLOT(newImageAccepted()));
    newImgDialog->exec();
}
void ImageViewer::newImageAccepted()
{
    NewImageDialog* newImgDialog = static_cast<NewImageDialog*>(sender());

    int width = newImgDialog->getWidth();
    int height = newImgDialog->getHeight();
    QString name = newImgDialog->getName();
    openNewTabForImg(new ViewerWidget(name, QSize(width, height)));
    ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);
}
void ImageViewer::on_actionOpen_triggered()
{
    QString folder = settings.value("folder_img_load_path", "").toString();

    QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
    QString fileName = QFileDialog::getOpenFileName(this, "Load image", folder, fileFilter);
    if (fileName.isEmpty()) { return; }

    QFileInfo fi(fileName);
    settings.setValue("folder_img_load_path", fi.absoluteDir().absolutePath());

    if (!openImage(fileName)) {
        msgBox.setText("Unable to open image.");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
}
void ImageViewer::on_actionSave_as_triggered()
{
    if (!isImgOpened()) {
        msgBox.setText("No image to save.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }
    QString folder = settings.value("folder_img_save_path", "").toString();

    ViewerWidget* w = getCurrentViewerWidget();

    QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
    QString fileName = QFileDialog::getSaveFileName(this, "Save image", folder + "/" + w->getName(), fileFilter);
    if (fileName.isEmpty()) { return; }

    QFileInfo fi(fileName);
    settings.setValue("folder_img_save_path", fi.absoluteDir().absolutePath());

    if (!saveImage(fileName)) {
        msgBox.setText("Unable to save image.");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
    else {
        msgBox.setText(QString("File %1 saved.").arg(fileName));
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
    }
}
void ImageViewer::on_actionClear_triggered()
{
    ui->horizontalSlider->setVisible(false);
    ui->timesteps->setVisible(false);
    ui->hvalue->setVisible(false);
    ui->tauvalue->setVisible(false);
    ui->omegavalue->setVisible(false);
    ui->SOR->setVisible(false);
    ui->PeronaMalik->setVisible(false);
    ui->Explicit->setVisible(false);

    ui->timestepslabel->setVisible(false);
    ui->hlabel->setVisible(false);
    ui->taulabel->setVisible(false);
    ui->omegalabel->setVisible(false);
    ui->pushButton->setVisible(false);
    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }
    clearImage();
}
void ImageViewer::on_actionInvert_colors_triggered() {
    ui->horizontalSlider->setVisible(false);
    ui->timesteps->setVisible(false);
    ui->hvalue->setVisible(false);
    ui->tauvalue->setVisible(false);
    ui->omegavalue->setVisible(false);
    ui->SOR->setVisible(false);
    ui->PeronaMalik->setVisible(false);
    ui->Explicit->setVisible(false);

    ui->timestepslabel->setVisible(false);
    ui->hlabel->setVisible(false);
    ui->taulabel->setVisible(false);
    ui->omegalabel->setVisible(false);
    ui->pushButton->setVisible(false);
    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }
    invertColors();
}

void ImageViewer::on_actionMirroring_triggered() {
    ui->horizontalSlider->setVisible(false);
    ui->timesteps->setVisible(false);
    ui->hvalue->setVisible(false);
    ui->tauvalue->setVisible(false);
    ui->omegavalue->setVisible(false);
    ui->SOR->setVisible(false);
    ui->PeronaMalik->setVisible(false);
    ui->Explicit->setVisible(false);

    ui->timestepslabel->setVisible(false);
    ui->hlabel->setVisible(false);
    ui->taulabel->setVisible(false);
    ui->omegalabel->setVisible(false);
    ui->pushButton->setVisible(false);
    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }

    //tu si zvoliim, ze z akeho obr chcem spravit
    QString folder = settings.value("folder_img_load_path", "").toString();
    //tu otvaram indput dialog na zadnie nka
    int n=QInputDialog::getInt(this,"Enter n",0,100);
    Mirroring(n,true);


}

void ImageViewer::on_actionHistogram_triggered() {

    ui->horizontalSlider->setVisible(false);
    ui->timesteps->setVisible(false);
    ui->hvalue->setVisible(false);
    ui->tauvalue->setVisible(false);
    ui->omegavalue->setVisible(false);
    ui->SOR->setVisible(false);
    ui->PeronaMalik->setVisible(false);
    ui->Explicit->setVisible(false);

    ui->timestepslabel->setVisible(false);
    ui->hlabel->setVisible(false);
    ui->taulabel->setVisible(false);
    ui->omegalabel->setVisible(false);
    ui->pushButton->setVisible(false);
    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }

    //true je pre vykreslenie aj
    histogram(true);


}

void ImageViewer::on_actionHistogram_Normal_triggered() {

    ui->horizontalSlider->setVisible(false);
    ui->timesteps->setVisible(false);
    ui->hvalue->setVisible(false);
    ui->tauvalue->setVisible(false);
    ui->omegavalue->setVisible(false);
    ui->SOR->setVisible(false);
    ui->PeronaMalik->setVisible(false);
    ui->Explicit->setVisible(false);

    ui->timestepslabel->setVisible(false);
    ui->hlabel->setVisible(false);
    ui->taulabel->setVisible(false);
    ui->omegalabel->setVisible(false);
    ui->pushButton->setVisible(false);
    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }

    //true je pre vykreslenie aj
    histogramN(true);


}

void ImageViewer::on_actionFSHS_triggered() {

    ui->horizontalSlider->setVisible(false);
    ui->timesteps->setVisible(false);
    ui->hvalue->setVisible(false);
    ui->tauvalue->setVisible(false);
    ui->omegavalue->setVisible(false);
    ui->SOR->setVisible(false);
    ui->PeronaMalik->setVisible(false);
    ui->Explicit->setVisible(false);

    ui->timestepslabel->setVisible(false);
    ui->hlabel->setVisible(false);
    ui->taulabel->setVisible(false);
    ui->omegalabel->setVisible(false);
    ui->pushButton->setVisible(false);
    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }

    fshs();


}

void ImageViewer::on_actionConvolution_triggered() {


    ui->horizontalSlider->setVisible(false);
    ui->timesteps->setVisible(false);
    ui->hvalue->setVisible(false);
    ui->tauvalue->setVisible(false);
    ui->omegavalue->setVisible(false);
    ui->SOR->setVisible(false);
    ui->PeronaMalik->setVisible(false);
    ui->Explicit->setVisible(false);

    ui->timestepslabel->setVisible(false);
    ui->hlabel->setVisible(false);
    ui->taulabel->setVisible(false);
    ui->omegalabel->setVisible(false);
    ui->pushButton->setVisible(false);

    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }

    //tu si zvoliim, ze z akeho obr chcem spravit
    QString folder = settings.value("folder_img_load_path", "").toString();
    //tu otvaram indput dialog na zadnie nka
    int n=QInputDialog::getInt(this,"Enter n",0,100);

    Mirroring(n,false);



    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }

    //tu si zvoliim, ze z akeho masky file txt chcem spravit masku


    QString fileFilter = "Mask data (*.txt *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
    QString fileName1 = QFileDialog::getOpenFileName(this, "Load mask", folder, fileFilter);
    if (fileName1.isEmpty()) {return; }


    QFileInfo fi1(fileName1);

    convolution(n,fileName1);



}

void ImageViewer::on_actionISODATA_Tresholding_triggered() {

    ui->horizontalSlider->setVisible(false);
    ui->timesteps->setVisible(false);
    ui->hvalue->setVisible(false);
    ui->tauvalue->setVisible(false);
    ui->omegavalue->setVisible(false);
    ui->SOR->setVisible(false);
    ui->PeronaMalik->setVisible(false);
    ui->Explicit->setVisible(false);

    ui->timestepslabel->setVisible(false);
    ui->hlabel->setVisible(false);
    ui->taulabel->setVisible(false);
    ui->omegalabel->setVisible(false);
    ui->pushButton->setVisible(false);
    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }

    int *hist;
    hist = histogram(false);

    isodata(hist);


}

void ImageViewer::on_actionBernsen_Tresholding_triggered() {

    ui->horizontalSlider->setVisible(false);
    ui->timesteps->setVisible(false);
    ui->hvalue->setVisible(false);
    ui->tauvalue->setVisible(false);
    ui->omegavalue->setVisible(false);
    ui->SOR->setVisible(false);
    ui->timestepslabel->setVisible(false);
    ui->hlabel->setVisible(false);
    ui->taulabel->setVisible(false);
    ui->omegalabel->setVisible(false);
    ui->pushButton->setVisible(false);
    ui->PeronaMalik->setVisible(false);
    ui->Explicit->setVisible(false);

    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }

    //tu si zvoliim, ze z akeho obr chcem spravit

    QString folder = settings.value("folder_img_load_path", "").toString();

    QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";

    int n=QInputDialog::getInt(this,"Enter n",0,100);

    Mirroring(n,false);

    int r=0,cmin=0,bg=0;

    r=QInputDialog::getInt(this,"Enter radius",0,100);
    cmin=QInputDialog::getInt(this,"Enter cmin",0,100);
    bg=QInputDialog::getInt(this,"Enter bg",0,1);

    bernsen(n,r,cmin,bg);


}



void ImageViewer::on_actionHeat_EQ_triggered() {

    if (!isImgOpened()) {
        msgBox.setText("No image is opened.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
        return;
    }
    ui->horizontalSlider->setVisible(true);
    ui->timesteps->setVisible(true);
    ui->hvalue->setVisible(true);
    ui->tauvalue->setVisible(true);
    ui->timestepslabel->setVisible(true);
    ui->hlabel->setVisible(true);
    ui->taulabel->setVisible(true);
    ui->pushButton->setVisible(true);
    ui->SOR->setVisible(true);
    ui->PeronaMalik->setVisible(true);
    ui->Explicit->setVisible(true);
    ui->Explicit->setChecked(true);




}

void ImageViewer::on_horizontalSlider_sliderMoved(int position)
{

    ViewerWidget* w = getCurrentViewerWidget();

    w->setUpdatesEnabled(true);
    w->setImage(sol[position]);
    w->update();
}



void ImageViewer::on_pushButton_clicked()
{
    double tau=0,h=0;
    int poc=0;

    poc=ui->timesteps->value();
    ui->horizontalSlider->setMaximum(poc-1);
    h=ui->hvalue->value();
    tau=ui->tauvalue->value();
    //ui->horizontalSlider->set;
    if(ui->SOR->isChecked()==true){
        double omega=0;
        omega=ui->omegavalue->value();
        sol=heatSOR(poc,h,tau,omega);
    }
    else if(ui->PeronaMalik->isChecked()==true)
        sol=perona_malikE(poc,h,tau);
    else if(ui->Explicit->isChecked()==true)
        sol=heatE(poc,h,tau);


}


void ImageViewer::on_SOR_clicked()
{
    if(ui->SOR->isChecked()==true){
        ui->omegalabel->setVisible(true);
        ui->omegavalue->setVisible(true);
    }
    else{
        ui->omegalabel->setVisible(false);
        ui->omegavalue->setVisible(false);
    }
    ui->Explicit->setChecked(false);
    ui->PeronaMalik->setChecked(false);


}


void ImageViewer::on_Explicit_clicked()
{
    ui->SOR->setChecked(false);
    ui->PeronaMalik->setChecked(false);
    ui->omegalabel->setVisible(false);
    ui->omegavalue->setVisible(false);

}

void ImageViewer::on_PeronaMalik_clicked()
{
    ui->SOR->setChecked(false);
    ui->Explicit->setChecked(false);
    ui->omegalabel->setVisible(false);
    ui->omegavalue->setVisible(false);
}
void ImageViewer::on_actionPerona_Malik_Reg_triggered() {
    ui->horizontalSlider->setVisible(true);
    double tau=0,h=0;
    int poc=0;

    poc = QInputDialog::getInt(this, "Enter Number of Time Steps", 0, 60);
    h = QInputDialog::getDouble(this, "Enter h", 0, 1, 0, 5, 4);
    tau = QInputDialog::getDouble(this, "Enter tau", 0, 0.25, 0.0, 0.25, 4);
    sol=perona_malikRegSOR(poc,h,tau,1.1);

    ui->horizontalSlider->setMaximum((2*poc)-1);


}
void ImageViewer::on_actionCurvature_Filter_triggered() {
    ui->horizontalSlider->setVisible(true);
    //ui->pushButton->setVisible(true);
    double tau=0,h=0;
    int poc=0;

    poc = QInputDialog::getInt(this, "Enter Number of Time Steps", 0, 60);
    h = QInputDialog::getDouble(this, "Enter h", 0, 1, 0, 5, 4);
    tau = QInputDialog::getDouble(this, "Enter tau", 0, 0.25, 0.0, 5., 4);

//    sol=curvature_filterSOR(51,1,0.05,1.1);
    sol=curvature_filterSOR(poc,h,tau,1.1);

    ui->horizontalSlider->setMaximum((2*poc)-1);

}

void ImageViewer::on_actionGMCF_triggered() {
    ui->horizontalSlider->setVisible(true);
    //ui->pushButton->setVisible(true);
    double tau=0,h=0;
    int poc=0;

    poc = QInputDialog::getInt(this, "Enter Number of Time Steps", 0, 60);
    h = QInputDialog::getDouble(this, "Enter h", 0, 1, 0, 5, 4);
    tau = QInputDialog::getDouble(this, "Enter tau", 0, 0.25, 0.0, 5., 4);

//    sol=curvature_filterSOR(51,1,0.05,1.1);
    sol=geodesic_curvature_filterSOR(poc,h,tau,1.1);

    ui->horizontalSlider->setMaximum((2*poc)-1);

}

void ImageViewer::on_actionEvolution_triggered() {
    ui->horizontalSlider->setVisible(true);
    //ui->pushButton->setVisible(true);
    double tau=0,h=0;
    int poc=0;

    poc = QInputDialog::getInt(this, "Enter Number of Time Steps", 0, 60);
    h = QInputDialog::getDouble(this, "Enter h", 0, 1, 0, 5, 4);
    tau = QInputDialog::getDouble(this, "Enter tau", 0, 0.25, 0.0, 5., 4);

    QString folder = settings.value("folder_img_load_path", "").toString();
    QString fileFilter = "DistanceFunction(*.txt *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
    QString fileName1 = QFileDialog::getOpenFileName(this, "Load mask", folder, fileFilter);
    if (fileName1.isEmpty()) {return; }

    QFileInfo fi1(fileName1);

    ZeroLineEvolutionSOR(poc,h,tau,1.1,fileName1);

}
void ImageViewer::on_actionSUBSURF_triggered() {
    //ui->horizontalSlider->setVisible(true);
    //ui->pushButton->setVisible(true);
    double tau=0,h=0;
    int poc=0;

    QString folder = settings.value("folder_img_load_path", "").toString();
    QString fileFilter = "Choose Level Set Function(*.txt *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
    QString fileName1 = QFileDialog::getOpenFileName(this, "Choose Level Set Function", folder, fileFilter);
    if (fileName1.isEmpty()) {return; }
    QFileInfo fi1(fileName1);

    folder = settings.value("folder_img_load_path", "").toString();
    fileFilter = "Choose Image(*.txt *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
    QString fileName2 = QFileDialog::getOpenFileName(this, "Choose Image", folder, fileFilter);
    if (fileName2.isEmpty()) {return; }
    QFileInfo fi2(fileName2);

    poc = QInputDialog::getInt(this, "Enter Number of Time Steps", 0, 60);
    h = QInputDialog::getDouble(this, "Enter h", 0, 1, 0, 5, 4);
    tau = QInputDialog::getDouble(this, "Enter tau", 0, 0.25, 0.0, 5., 4);

    Segmentation_SUBSURF_Bernsen(poc,h,tau,1.1,fileName1,fileName2);

    //ui->horizontalSlider->setMaximum((2*poc)-1);

}
