#pragma once

#include <QtWidgets/QMainWindow>
//#include <QtWidgets>
#include "ui_ImageViewer.h"
#include "ViewerWidget.h"
#include "NewImageDialog.h"

class ImageViewer : public QMainWindow
{
	Q_OBJECT

public:
	ImageViewer(QWidget* parent = Q_NULLPTR);

private:
	Ui::ImageViewerClass* ui;
	NewImageDialog* newImgDialog;

	QSettings settings;
	QMessageBox msgBox;
    QImage *sol;

	//ViewerWidget functions
	ViewerWidget* getViewerWidget(int tabId);
	ViewerWidget* getCurrentViewerWidget();

	//Event filters
	bool eventFilter(QObject* obj, QEvent* event);




	//ViewerWidget Events
	bool ViewerWidgetEventFilter(QObject* obj, QEvent* event);
	void ViewerWidgetMouseButtonPress(ViewerWidget* w, QEvent* event);
	void ViewerWidgetMouseButtonRelease(ViewerWidget* w, QEvent* event);
	void ViewerWidgetMouseMove(ViewerWidget* w, QEvent* event);
	void ViewerWidgetLeave(ViewerWidget* w, QEvent* event);
	void ViewerWidgetEnter(ViewerWidget* w, QEvent* event);
	void ViewerWidgetWheel(ViewerWidget* w, QEvent* event);

	//ImageViewer Events
	void closeEvent(QCloseEvent* event);

	//Image functions
	void openNewTabForImg(ViewerWidget* vW);
	bool openImage(QString filename);
	bool saveImage(QString filename);
	bool clearImage();
	bool invertColors();
    bool Mirroring(int n,bool b);
    int* histogram(bool b);
    double* histogramN(bool b);
    bool fshs();
    bool convolution(int n,QString filename);
    double *loadmask(QString filename);
    int isodata(int *hist);
    int mean(int *hist,int a , int b,int N);
    int count(int *hist,int a, int b);
    bool bernsen(int n, int r, int cmin, int bg);
    int max(int *maska,int size);
    int min(int *maska,int size);
    QImage* heatE(int poc, double h, double tau);
    QImage* heatSOR(int poc, double h, double tau,double omega);
    QImage* perona_malikE(int poc, double h, double tau);
    double  gFunction(double s);
    double  gRegFunction(double s);
    double gradNorm(double ynw,double yne,double ysw,double yse,double xw,double xe,double h);
    double* mirror(int n,double* img,int x , int y);
    double* linearDiffusionSOR(double* u0,int x1, int x2,int poc, double h, double tau,double omega,bool ret);
    QImage* perona_malikRegSOR(int poc, double h, double tau, double omega);
    QImage* curvature_filterSOR(int poc, double h, double tau, double omega);
    double* gRegArray(double *u0,int x1, int x2,double h);
    QImage* geodesic_curvature_filterSOR(int poc, double h, double tau, double omega);
    void ZeroLineEvolutionSOR(int poc, double h, double tau, double omega,QString filename);
    double *loadDF(QString filename);
    QImage* Segmentation_SUBSURF_Bernsen(int poc, double h, double tau, double omega,QString filenameLSF,QString filenameOI);





	//Inline functions
	inline bool isImgOpened() { return ui->tabWidget->count() == 0 ? false : true; }

private slots:
	//Tabs slots
	void on_tabWidget_tabCloseRequested(int tabId);
	void on_actionRename_triggered();

	//Image slots
	void on_actionNew_triggered();
	void newImageAccepted();
	void on_actionOpen_triggered();
	void on_actionSave_as_triggered();
	void on_actionClear_triggered();
	void on_actionInvert_colors_triggered();
    void on_actionMirroring_triggered();
    void on_actionHistogram_triggered();
    void on_actionHistogram_Normal_triggered();
    void on_actionFSHS_triggered();
    void on_actionConvolution_triggered();
    void on_actionISODATA_Tresholding_triggered();
    void on_actionBernsen_Tresholding_triggered();
    void on_actionHeat_EQ_triggered();
    void on_horizontalSlider_sliderMoved(int position);
    void on_pushButton_clicked();
    void on_SOR_clicked();
    void on_Explicit_clicked();
    void on_PeronaMalik_clicked();
    void on_actionPerona_Malik_Reg_triggered();
    void on_actionCurvature_Filter_triggered();
    void on_actionGMCF_triggered();
    void on_actionEvolution_triggered();

    void on_actionSUBSURF_triggered();




};


